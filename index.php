<!doctype html>
<html lang="ja">
<head>
<?php include("common/inc/head.php"); ?>
<title>kitano garden</title>
<!-- ▼個別CSS▼ -->
<link rel="stylesheet" type="text/css" href="/index/styles/styles.css">
<link rel="stylesheet" href="//cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
<link rel="stylesheet" type="text/css" href="/common/styles/jquery.bxslider/jquery.bxslider.css">
<!-- ▲個別CSS▲ -->
<!-- ▼個別JS▼ -->
<script type="text/javascript" src="/index/js/index.js"></script>
<script type="text/javascript" src="/common/js/jquery.matchHeight/jquery.matchHeight.js"></script>
<script src="//cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="/common/js/jquery.bxslider/bxslider.js"></script>
<!-- ▲個別JS▲ -->
</head>
<body>
<?php include("common/inc/header.php"); ?>
<div class="l-mvBlock">
    <div class="l-mv">
        <div class="l-inner">
        	<img class="is-imgChange" src="/index/img/img_mv_pc.png" alt="Kitano Garden Wedding 気取らない飾らない、でも特別">
            <p>気取らない飾らない、でも特別</p>
        </div>        
    </div>
</div>
    
<div id="wrapper">
    <section>
    <div class="l-block01">
    	<h2><img class="is-imgChange" src="/index/img/h2_block01_pc.png" alt="CONCEPT はじめに"><span><span class="Cinzel">Concept</span>はじめに</span></h2>
    	<div class="l-block01-table">
        	<div class="l-message">
                <p>
                	<span>気取らない飾らない、でも特別</span>
                    北野ガーデンは北野坂沿いの広々とした敷地に、チャペル・ガーデンパーティ会場・レストラン・ブライダルサロンを擁する、ウェディングステージです。緑に抱かれてたたずむ白いチャペル。１年中青々とした芝が美しい、自然の光が柔らかく彩るパーティスペースやフレンチレストランなど。この広大なステージでお二人のスタイルにあわせたウェディングシーンが叶います。
                </p>
            </div>
            <div class="l-image">
                <img class="is-imgChange" src="/index/img/img_block01_pc.png" alt="">
            </div>
        </div>
    </div>
    </section>
    
    <section>
    <div class="l-block02">
    	<div class="l-inner">
            <h2><img class="is-imgChange" src="/index/img/h2_block02_pc.png" alt="SEASON 春夏秋冬"><span><span class="Cinzel">Season</span>春夏秋冬</span></h2>
            <div class="l-message">
                <p class="p-title">四季折々の自然に囲まれて</p>
                <p class="p-message">
                    「春・夏・秋・冬」<br>季節ごとに違った表情を見せてくれるガーデンは情緒があり感動的。<br class="pconly2">お二人の最高のウェディングステージを演出します。
                </p>
                <p class="pconly2"><a class="p-btn" href="/season/">もっと見る</a></p>
            </div>
            <div class="l-block02-table">
                <div>
                    <p><a href="/season/#01"><img src="/index/img/img_block02_01_pc.png" alt="SPRING"><span class="Cinzel">Spring</span></a></p>
                </div><div>
                    <p><a href="/season/#02"><img src="/index/img/img_block02_02_pc.png" alt="SUMMER"><span class="Cinzel">Summer</span></a></p>
                </div><div>
                    <p><a href="/season/#03"><img src="/index/img/img_block02_03_pc.png" alt="AUTUMN"><span class="Cinzel">Autumn</span></a></p>
                </div><div>
                    <p><a href="/season/#04"><img src="/index/img/img_block02_04_pc.png" alt="WINTER"><span class="Cinzel">Winter</span></a></p>
                </div>
            </div>
            <p class="sponly1000"><a class="p-btn" href="/season/">もっと見る</a></p>
        </div>
    </div>
    </section>
    
    <section>
    <div class="l-block03">
        <div class="l-inner">
        	<h2><img class="is-imgChange" src="/index/img/h2_block03_pc.png" alt="CEREMONY&PARTY 挙式&披露パーティ"><span><span class="Cinzel">CEREMONY&amp;PARTY</span>挙式&amp;披露パーティ</span></h2>
            <div class="l-block03-table">
            	<p><a href="/ceremony/"><img src="/index/img/img_block03_01_pc.png" alt="CEREMONY"><span class="Cinzel">Ceremony</span></a></p>
                <p><a href="/party/"><img src="/index/img/img_block03_02_pc.png" alt="PARTY"><span class="Cinzel">Party</span></a></p>
            </div>
        </div>
    </div>
    </section>
    
    <section>
    <div class="l-block04">
    	<div class="l-inner">
    		<h2><img class="is-imgChange" src="/index/img/h2_block04_pc.png" alt="FAIR ブライダルフェア"><span><span class="Cinzel">Fair</span>ブライダルフェア</span></h2>
            <div class="l-block04-table01">
            	<div id="l-calender01" class="l-calender01">
                	<p class="p-calender01 p-current">CALENDER<span>カレンダーから選ぶ</span></p>
                </div><div id="l-category01" class="l-category01">
                	<p class="p-category01">CATEGORY<span>内容から選ぶ</span></p>
                </div>            
            </div>
            <div class="l-block04-table02">
            	<div class="l-calender02">
                	<img src="/index/img/img_block04_01_pc.png" alt="">
                </div><div class="l-category02">
                	<p class="p-title">下記カテゴリーからお選びください</p>
                    <div class="l-category02-table">
                    	<div>
                        	<p class="p-image"><a href=""></a></p>
                            <p class="p-message">料理試食付き</p>
                    	</div><div>
                        	<p class="p-image"><a href=""></a></p>
                            <p class="p-message">特典付き</p>
                    	</div><div>
                        	<p class="p-image"><a href=""></a></p>
                            <p class="p-message">チャペル見学&amp;体験</p>
                    	</div><div>
                        	<p class="p-image"><a href=""></a></p>
                            <p class="p-message">会場コーディネート&amp;ドレス展示</p>
                    	</div><div>
                        	<p class="p-image"><a href=""></a></p>
                            <p class="p-message">18:00以降もOK</p>
                    	</div><div>
                        	<p class="p-image"><a href=""></a></p>
                            <p class="p-message">土日祝開催のフェア</p>
                    	</div>
                    </div>
                </div>            
            </div>
            <div class="l-btn">
                <p>
                    <a href="/fair/">フェア一覧を見る<span>BRIDAL FAIR</span></a>
                </p>
            </div>
    	</div>
    </div>
    </section>
    
    <section>
    <div class="l-block05">
    	<div class="l-inner">
    		<div class="l-block05-1-table">
                <div class="l-block05-1">
                	<p class="p-title"><img class="is-imgChange" src="/index/img/h2_block05_pc.png" alt="PLAN ウェディングプラン"><span><span class="Cinzel">Plan</span>ウェディングプラン</span></p>
                    <p class="p-message">
                    	スタイルや目的別に合わせて、計10種のお得なプランをご用意。お料理やドリンクはもちろん、衣装や美容・花や引出物など充実の内容です。<br>
                        お2人にぴったりのプランが見つかるはず♪
                    </p>
                    <div class="l-btn">
                        <p>
                            <a href="/plan/">プラン一覧を見る<span>WEDDING PLAN</span></a>
                        </p>
                    </div>
                </div>            
                <div class="l-block05-2">
                	<div class="l-slider">
                        <div id="slider9">
                            <div class="l-plan">
                                <div class="l-image">
                                    <p class="p-pickup"><img src="/index/img/ico_block05_1_pc.png" alt="PICK UP"></p>
                                    <p class="p-image"><img src="/index/img/img_block05_01_pc.png" alt=""></p>
                                </div>
                                <div class="l-message">
                                    <p class="p-title">【1軒目来館限定】スペシャル特典</p>
                                    <p class="p-message">
                                        【１軒目来館がオトク】<br>
                                        来館＆成約特典付きのスペシャル特典が期間限定で登場！初めての会場見学は『北野ガーデン』へ
                                    </p>
                                    <p class="p-kikan">期間：2018年12月22日以降の来館・成約者限定</p>
                                    <p><a class="p-common-btn2" href="/plan/#01">もっと見る</a></p>
                                </div>
                            </div>
                            <div class="l-plan">
                                <div class="l-image">
                                    <p class="p-pickup"><img src="/index/img/ico_block05_1_pc.png" alt="PICK UP"></p>
                                    <p class="p-image"><img src="/index/img/img_block05_02_pc.png" alt=""></p>
                                </div>
                                <div class="l-message">
                                    <p class="p-title">【2軒目来館限定】スペシャル特典</p>
                                    <p class="p-message">
                                        【１軒目来館がオトク】<br>
                                        来館＆成約特典付きのスペシャル特典が期間限定で登場！初めての会場見学は『北野ガーデン』へ
                                    </p>
                                    <p class="p-kikan">期間：2018年12月22日以降の来館・成約者限定</p>
                                    <p><a class="p-common-btn2" href="/plan/#02">もっと見る</a></p>
                                </div>
                            </div>
                            <div class="l-plan">
                                <div class="l-image">
                                    <p class="p-pickup"><img src="/index/img/ico_block05_1_pc.png" alt="PICK UP"></p>
                                    <p class="p-image"><img src="/index/img/img_block05_02_pc.png" alt=""></p>
                                </div>
                                <div class="l-message">
                                    <p class="p-title">【3軒目来館限定】スペシャル特典</p>
                                    <p class="p-message">
                                        【１軒目来館がオトク】<br>
                                        来館＆成約特典付きのスペシャル特典が期間限定で登場！初めての会場見学は『北野ガーデン』へ
                                    </p>
                                    <p class="p-kikan">期間：2018年12月22日以降の来館・成約者限定</p>
                                    <p><a class="p-common-btn2" href="/plan/#03">もっと見る</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="l-block05-3 sponly1000">
                <div class="l-btn">
                    <p>
                        <a href="/plan/">プラン一覧を見る<span>WEDDING PLAN</span></a>
                    </p>
                </div>
            </div>
    	</div>
    </div>
    </section>
    
    <section>
    <div class="l-block06">
    	<div class="l-inner">
        	<h2><img class="is-imgChange" src="/index/img/h2_block06_pc.png" alt="STYLE ウェディングスタイル"><span><span class="Cinzel">Style</span>ウェディングスタイル</span></h2>
            <div class="l-block06-01">
            	<a class="p-common-btn" href="/weddingstyle/">WEDDING STYLEを見る</a>
            </div>
            <div class="l-block06-02">
            	<div>
                	<p class="p-title"><img class="is-imgChange" src="/index/img/h3_block06_1_pc.png" alt="DRESS ドレス"></p>
                    <p class="p-image"><img src="/index/img/img_block06_02_1_pc.png" alt="DRESS ドレス"></p>
                    <p><a class="p-common-btn" href="">もっと見る</a></p>
                </div><div>
                	<p class="p-title"><img class="is-imgChange" src="/index/img/h3_block06_2_pc.png" alt="CUISINE 料理"></p>
                    <p class="p-image"><img src="/index/img/img_block06_02_2_pc.png" alt="CUISINE 料理"></p>
                    <p><a class="p-common-btn" href="/cuisine/">もっと見る</a></p>                
                </div><div>
                	<p class="p-title"><img class="is-imgChange" src="/index/img/h3_block06_3_pc.png" alt="FLOWER 装花"></p>
                    <p class="p-image"><img src="/index/img/img_block06_02_3_pc.png" alt="FLOWER 装花"></p>
                    <p><a class="p-common-btn" href="/flower/">もっと見る</a></p>                
                </div>
            </div>
        </div>    
    </div>
    </section>
    
    <section>
    <div class="l-block07">
    	<div class="l-block07-table">
        	<div class="l-message">
                <h2><img class="is-imgChange" src="/index/img/h2_block07_pc.png" alt="FLOW 結婚式までの流れ"><span><span class="Cinzel">Flow</span>結婚式までの流れ</span></h2>
                <p class="p-message">
                	最高の一日を迎えるための<br>会場見学から結婚準備・当日までの流れ
                </p>
                <p class="pconly2"><a class="p-common-btn" href="/flow/">もっと見る</a></p>
            </div>
            <div class="l-image">
                <img class="is-imgChange" src="/index/img/img_block07_pc.png" alt="">
            </div>
        </div>
        <p class="sponly1000"><a class="p-common-btn" href="/flow/">もっと見る</a></p>
    </div>
    </section>
    
    <section>
    <div class="l-block08">    	
        <div class="l-block08-01">
            <div class="l-block08-table">
                <div>
                    <h2><img class="is-imgChange" src="/index/img/h2_block08_pc.png" alt="PHOTO GALLERY フォトギャラリー"><span><span class="Cinzel">Photo Gallery</span>フォトギャラリー</span></h2>            
                    <a href="/gallery/"><img class="is-imgChange" src="/index/img/btn_block08_pc.png" alt="もっと見る"></a>
                </div>
            </div>
        </div>
    </div>
    </section>
    
</div>
<?php include("common/inc/footer.php"); ?>
</body>
</html>
