<!doctype html>
<html lang="ja">
<head>
<?php include("../common/inc/head.php"); ?>
<title>ACCESS アクセス｜kitano garden</title>
<!-- ▼個別CSS▼ -->
<link rel="stylesheet" type="text/css" href="/guest/common/styles/guest.css">
<link rel="stylesheet" type="text/css" href="/common/styles/lightbox/lightbox.css">
<!-- ▲個別CSS▲ -->
<!-- ▼個別JS▼ -->
<script type="text/javascript" src="/guest/common/js/guest.js"></script>
<script type="text/javascript" src="/common/js/jquery.matchHeight/jquery.matchHeight.js"></script>
<script type="text/javascript" src="/common/js/lightbox/lightbox.js"></script>
<!-- ▲個別JS▲ -->
</head>
<body>
<?php include("../common/inc/header.php"); ?>
<div class="l-mvBlock">
    <div class="l-mv">
    	
    </div>
</div>    
<div id="wrapper">
    <section>
    <div class="l-block01">
    	<div class="l-inner">
            <h2><img class="is-imgChange" src="/guest/common/img/h2_block01_pc.png" alt="FOR GUEST ゲストのみなさまへ"><span><span class="Cinzel">For Guest</span>ゲストのみなさまへ</span></h2>
            <div class="l-block01-table">
                <p class="p-btn"><a class="is-pagescroll" href="#01">アクセス</a></p>
                <p class="p-btn"><a class="is-pagescroll" href="#02">ゲスト用施設</a></p>
                <p class="p-btn"><a class="is-pagescroll" href="#03">よくあるご質問</a></p>
            </div>
        </div>
    </div>
    </section>
    
    <section>
    <div class="l-block02">
    	<div id="01" class="l-inner">
            <h3><span class="Cinzel">Access</span>アクセス</h3>
            <div class="l-block02-map">
            	<img class="is-imgChange" src="/access/common/img/img_block01_pc.png" alt="kitano garden MAP">
            </div>
            <p class="p-googlemap"><a class="p-common-btn" href="https://goo.gl/maps/SKYfDqAojR32" target="_blank">GOOGLE MAP</a></p>
        	<p class="p-message">
            	〒650-0002 神戸市中央区北野町2-8-1<br>
                【TEL】078-241-8537<br>
                【営業時間】11:00～19:00<br>
                【定休日】水曜定休
            </p>
            <div class="l-block02-table">
                <div>
                    <p class="p-title">電車にてお越しの場合</p>
                    <p class="p-message">
                        JR・阪神・阪急各線「三宮」駅下車　徒歩12分<br>
                        JR新幹線・地下鉄各線「新神戸」駅下車　徒歩10分
                    </p>
                </div>
                <div>
                    <p class="p-title">お車にてお越しの場合</p>
                    <p class="p-message">
                        阪神高速道路「京橋」「生田川」ランプより約5分<br>
                        提携駐車場をご用意しております。詳しくはパーキングマップをご覧ください。<br>
                        提携駐車場は数に限りがございます。できるだけ他の交通機関をご利用くださいますようお願い申し上げます。
                    </p>
                    <p class="p-btn"><a class="p-common-btn2" href="" target="_blank">PARKING MAP</a></p>
                </div>
                <div>
                    <p class="p-title">神戸空港からお越しの場合</p>
                    <p class="p-message">
                        ポートライナー「三宮」駅下車 徒歩約12分
                    </p>
                </div>
            </div>
        </div>
    </div>
    </section>
    
    <section>
    <div class="l-block03">
    	<div id="02" class="l-inner">
            <h3><span class="Cinzel">Facility</span>ゲスト用施設</h3>
            <div class="l-block03-table">
            	<div>
                	<p class="p-image"><img src="/guest/common/img/img_block03_01_pc.png" alt="待合スペース（1F）"></p>
                    <p class="p-message">待合スペース（1F）</p>
                </div><div>
                	<p class="p-image"><img src="/guest/common/img/img_block03_02_pc.png" alt="待合スペース（2F）"></p>
                    <p class="p-message">待合スペース（2F）</p>
                </div><div>
                	<p class="p-image"><img src="/guest/common/img/img_block03_03_pc.png" alt="ご親族様用控室"></p>
                    <p class="p-message">ご親族様用控室</p>
                </div><div>
                	<p class="p-image"><img src="/guest/common/img/img_block03_04_pc.png" alt="ゲスト用着替室"></p>
                    <p class="p-message">ゲスト用着替室</p>
                </div>
            </div>
        </div>
    </div>
    </section>
    
    <section>
    <div class="l-block04">
    	<div id="03" class="l-inner">
            <h3><span class="Cinzel">Faq</span>よくあるお問い合わせ</h3>
            <div class="l-block04-table">
            	<div>
                	<p class="p-question">
                    	<span class="Cinzel">Q</span>
                        <span>クロークはありますか？</span>
                        <span class="p-sub js-menu">＋</span></p>
                    <div class="l-answer">
                        <p class="p-answer">
                            <span class="Cinzel"></span>
                            <span>クロークを用意しておりますので、お荷物をお預けいただけます。<br>（貴重品は併設のコインロッカーをご利用くださいませ）</span>
                        </p>
                    </div>
                </div>
                <div>
                	<p class="p-question">
                    	<span class="Cinzel">Q</span>
                        <span>ゲスト用の待合室はありますか？</span>
                        <span class="p-sub js-menu">＋</span></p>
                    <div class="l-answer">
                        <p class="p-answer">
                            <span class="Cinzel"></span>
                            <span>はい、館内に専用のお部屋を用意してございます。</span>
                        </p>
                    </div>
                </div>
                <div>
                	<p class="p-question">
                    	<span class="Cinzel">Q</span>
                        <span>親族用の控え室はありますか？</span>
                        <span class="p-sub js-menu">＋</span></p>
                    <div class="l-answer">
                        <p class="p-answer">
                            <span class="Cinzel"></span>
                            <span>はい、館内に専用のお部屋を用意してございます。</span>
                        </p>
                    </div>
                </div>
                <div>
                	<p class="p-question">
                    	<span class="Cinzel">Q</span>
                        <span>ゲスト用の着替え室はありますか？</span>
                        <span class="p-sub js-menu">＋</span></p>
                    <div class="l-answer">
                        <p class="p-answer">
                            <span class="Cinzel"></span>
                            <span>はい、館内に専用のお部屋を用意してございます。</span>
                        </p>
                    </div>
                </div>
                <div>
                	<p class="p-question">
                    	<span class="Cinzel">Q</span>
                        <span>留袖やモーニングなど、親族の衣装や子供の衣装レンタルはできますか？</span>
                        <span class="p-sub js-menu">＋</span></p>
                    <div class="l-answer">
                        <p class="p-answer">
                            <span class="Cinzel"></span>
                            <span>はい。提携衣装店にて多数ご用意をしております。</span>
                        </p>
                    </div>
                </div>
                <div>
                	<p class="p-question">
                    	<span class="Cinzel">Q</span>
                        <span>当日の着付けやヘアメイクはお願いできますか？</span>
                        <span class="p-sub js-menu">＋</span></p>
                    <div class="l-answer">
                        <p class="p-answer">
                            <span class="Cinzel"></span>
                            <span>はい、可能です。お着付けやヘアセット、ヘアメイクなども承っております。<br>お気軽にお問い合わせくださいませ。</span>
                        </p>
                    </div>
                </div>
                <div>
                	<p class="p-question">
                    	<span class="Cinzel">Q</span>
                        <span>親族紹介はできますか？</span>
                        <span class="p-sub js-menu">＋</span></p>
                    <div class="l-answer">
                        <p class="p-answer">
                            <span class="Cinzel"></span>
                            <span>はい、可能です。</span>
                        </p>
                    </div>
                </div>
                <div>
                	<p class="p-question">
                    	<span class="Cinzel">Q</span>
                        <span>宿泊はできますか？</span>
                        <span class="p-sub js-menu">＋</span></p>
                    <div class="l-answer">
                        <p class="p-answer">
                            <span class="Cinzel"></span>
                            <span>提携の宿泊施設をご紹介させていただきます。</span>
                        </p>
                    </div>
                </div>
                <div>
                	<p class="p-question">
                    	<span class="Cinzel">Q</span>
                        <span>アレルギーをもっているのですが、大丈夫ですか？</span>
                        <span class="p-sub js-menu">＋</span></p>
                    <div class="l-answer">
                        <p class="p-answer">
                            <span class="Cinzel"></span>
                            <span>アレルギー方には特別料理をご用意させていただいております。招待状のご返信にお書きいただくか、新郎新婦様にお声掛けくださいませ。</span>
                        </p>
                    </div>
                </div>
                <div>
                	<p class="p-question">
                    	<span class="Cinzel">Q</span>
                        <span>余興のことで相談したいのですが？</span>
                        <span class="p-sub js-menu">＋</span></p>
                    <div class="l-answer">
                        <p class="p-answer">
                            <span class="Cinzel"></span>
                            <span>お気軽にご連絡ください。ご結婚式の担当プランナーまでお問い合わせくださいませ。</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
      
</div>
<?php include("../common/inc/footer.php"); ?>
</body>
</html>
