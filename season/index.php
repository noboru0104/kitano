    <!doctype html>
<html lang="ja">
<head>
<?php include("../common/inc/head.php"); ?>
<title>SEASON 春夏秋冬｜kitano garden</title>
<!-- ▼個別CSS▼ -->
<link rel="stylesheet" type="text/css" href="/season/common/styles/season.css">
<link rel="stylesheet" type="text/css" href="/common/styles/lightbox/lightbox.css">
<!-- ▲個別CSS▲ -->
<!-- ▼個別JS▼ -->
<script type="text/javascript" src="/season/common/js/season.js"></script>
<script type="text/javascript" src="/common/js/jquery.matchHeight/jquery.matchHeight.js"></script>
<script type="text/javascript" src="/common/js/lightbox/lightbox.js"></script>
<!-- ▲個別JS▲ -->
</head>
<body>
<?php include("../common/inc/header.php"); ?>
<div id="wrapper">
    <section>
        <div class="l-block01">
            <div class="l-inner">
                <div class="l-mv">
                    <h2 class="l-mv-ttl-en">Season</h2>
                    <p class="l-mv-ttl-jp">春夏秋冬</p>
                </div>
                <div class="l-message">
                    <p class="p-title">四季折々の自然に囲まれて</p>
                    <p class="p-message">
                        「春・夏・秋・冬」<br>季節ごとに違った表情を見せてくれるガーデンは情緒があり感動的。<br class="pconly2">お二人の最高のウェディングステージを演出します。
                    </p>
                </div>
            </div>
        </div>
    </section>
    
    <section>
        <div class="l-block02 p-spring">    	
            <p id="01" class="p-title"><span class="Cinzel">Spring</span>- 春 -</p>
            <p class="p-image"><img class="is-imgChange" src="/season/common/img/img_block02_1_pc.png" alt="SPRING 春"></p>
            <div class="l-block02-table l-spring">
                <div class="l-inner1160">
                    <div class="l-image">
                        <img src="/season/common/img/img_block02_2_pc.png" alt="いのち芽吹く季節、幸せのはじまり">
                    </div><div class="l-message">
                        <h3><img class="is-imgChange" src="/season/common/img/h3_block02_pc.png" alt="SPRING 春"><span>- 春 -</span></h3>
                        <div>
                            <p class="p-title">いのち芽吹く季節、幸せのはじまり</p>
                            <p class="p-message">やわらかな陽射しとともに桜が咲き乱れ<br>ガーデンの草花も顔をだします</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="l-block03 l-spring">
            <div class="l-block03-table">
                <div class="l-inner1160">
                	<div class="l-message">
                        <h4><img class="is-imgChange" src="/season/common/img/h4_block03_pc.png" alt="CUISINE 料理"><span><span class="Cinzel">Cuisine</span>料理</span></h4>
                        <div>
                            <p class="p-title">彩りの春 美しき春</p>
                            <p class="p-message">鮮やかな色彩のお料理と、<br class="sponly1000">春ならではの食材を贅沢に調理</p>
                        	<p class="p-btn pconly2"><a class="Cinzel" href="/cuisine/">料理を見る</a></p>
                        </div>
                    </div><div class="l-image">
                        <img src="/season/common/img/img_block03_1_pc.png" alt="彩りの春 美しき春"><br class="sponly1000">
                        <p class="p-btn sponly1000"><a class="Cinzel" href="/cuisine/">料理を見る</a></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="l-block04 l-spring">
        	<div class="l-inner1160">  	
            	<div class="l-block04-01">
                	<h4><span class="Cinzel">Spring Style</span>スタイル</h4>
                    <div class="l-block04-table">
                        <div>
                            <p class="p-image"><img src="/season/common/img/img_block04_1_pc.png" alt="春のコーディネイト"></p>
                            <p class="p-message">
                                春のコーディネイト
                            </p>
                        </div><div>
                            <p class="p-image"><img src="/season/common/img/img_block04_2_pc.png" alt="春めいたウェディングアイテム"></p>
                            <p class="p-message">
                                春めいたウェディングアイテム
                            </p>
                        </div><div>
                            <p class="p-image"><img src="/season/common/img/img_block04_3_pc.png" alt="春のデザート演出"></p>
                            <p class="p-message">
                                春のデザート演出
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section>
        <div class="l-block02 l-summer">    	
            <p id="02" class="p-title"><span class="Cinzel">Summer</span>- 夏 -</p>
            <p class="p-image"><img class="is-imgChange" src="/season/common/img/img_block05_1_pc.png" alt="SUMMER 夏"></p>
            <div class="l-block02-table">
                <div class="l-inner1160">
                    <div class="l-image">
                        <img src="/season/common/img/img_block05_2_pc.png" alt="滝の水音、新緑がまぶしい夏の訪れ">
                    </div><div class="l-message">
                        <h3><img class="is-imgChange" src="/season/common/img/h3_block05_pc.png" alt="SUMMER 夏"><span>- 夏 -</span></h3>
                        <div>
                            <p class="p-title">滝の水音、新緑がまぶしい夏の訪れ</p>
                            <p class="p-message">庭園をキラキラ輝かせ木漏れ日と揺れる葉<br>祝福の太陽が二人を照らします</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="l-block03 l-summer">    	
            <div class="l-block03-table">
                <div class="l-inner1160">
                	<div class="l-message">
                        <h4><img class="is-imgChange" src="/season/common/img/h4_block06_pc.png" alt="CUISINE 料理"><span><span class="Cinzel">Cuisine</span>料理</span></h4>
                        <div>
                            <p class="p-title">青と涼の夏 解き放たれる夏</p>
                            <p class="p-message">涼しげなお皿に盛られるお料理や<br>夏季だけの食材に出会える幸せの時間</p>
                        	<p class="p-btn pconly2"><a class="Cinzel" href="/cuisine/">料理を見る</a></p>
                        </div>
                    </div><div class="l-image">
                        <img src="/season/common/img/img_block06_1_pc.png" alt="青と涼の夏 解き放たれる夏"><br class="sponly1000">
                        <p class="p-btn sponly1000"><a class="Cinzel" href="/cuisine/">料理を見る</a></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="l-block04 l-summer">  
        	<div class="l-inner1160">  	
            	<div class="l-block04-01">
                	<h4><span class="Cinzel">Summer Style</span>スタイル</h4>
                    <div class="l-block04-table">
                        <div>
                            <p class="p-image"><img src="/season/common/img/img_block07_1_pc.png" alt="夏のコーディネイト"></p>
                            <p class="p-message">
                                夏のコーディネイト
                            </p>
                        </div><div>
                            <p class="p-image"><img src="/season/common/img/img_block07_2_pc.png" alt="夏ドレス"></p>
                            <p class="p-message">
                                夏ドレス
                            </p>
                        </div><div>
                            <p class="p-image"><img src="/season/common/img/img_block07_3_pc.png" alt="夏の乾杯"></p>
                            <p class="p-message">
                                夏の乾杯
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section>
        <div class="l-block02 l-autumn">
            <p id="03" class="p-title"><span class="Cinzel">Autumn</span>- 秋 -</p>
            <p class="p-image"><img class="is-imgChange" src="/season/common/img/img_block08_1_pc.png" alt="AUTUMN 秋"></p>
            <div class="l-block02-table">
                <div class="l-inner1160">
                    <div class="l-image">
                        <img src="/season/common/img/img_block08_2_pc.png" alt="紅く染まる心と木々、愛を誓う秋">
                    </div><div class="l-message">
                        <h3><img class="is-imgChange" src="/season/common/img/h3_block08_pc.png" alt="AUTUMN 秋"><span>- 秋 -</span></h3>
                        <div>
                            <p class="p-title">紅く染まる心と木々、愛を誓う秋</p>
                            <p class="p-message">グラデーションに染まる風景はまるで<br class="sponly1000">暖色カラーの刺繍のように</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="l-block03 l-autumn">	
            <div class="l-block03-table">
                <div class="l-inner1160">
                	<div class="l-message">
                        <h4><img class="is-imgChange" src="/season/common/img/h4_block09_pc.png" alt="CUISINE 料理"><span><span class="Cinzel">Cuisine</span>料理</span></h4>
                        <div>
                            <p class="p-title">味覚誇る秋 香りの秋</p>
                            <p class="p-message">旬の食材が豊かに取り揃う季節<br>香高き高貴なお料理でおもてなし</p>
                        	<p class="p-btn pconly2"><a class="Cinzel" href="/cuisine/">料理を見る</a></p>
                        </div>
                    </div><div class="l-image">
                        <img src="/season/common/img/img_block09_1_pc.png" alt="味覚誇る秋 香りの秋"><br class="sponly1000">
                        <p class="p-btn sponly1000"><a class="Cinzel" href="/cuisine/">料理を見る</a></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="l-block04 l-autumn">
        	<div class="l-inner1160">  	
            	<div class="l-block04-01">
                	<h4><span class="Cinzel">Autumn Style</span>スタイル</h4>
                    <div class="l-block04-table">
                        <div>
                            <p class="p-image"><img src="/season/common/img/img_block10_1_pc.png" alt="秋のコーディネイト"></p>
                            <p class="p-message">
                                秋のコーディネイト
                            </p>
                        </div><div>
                            <p class="p-image"><img src="/season/common/img/img_block10_2_pc.png" alt="秋のBGM"></p>
                            <p class="p-message">
                                秋のBGM
                            </p>
                        </div><div>
                            <p class="p-image"><img src="/season/common/img/img_block10_3_pc.png" alt="秋のガーデンパーティー"></p>
                            <p class="p-message">
                                秋のガーデンパーティー
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section>
        <div class="l-block02 l-winter">    	
            <p id="04" class="p-title"><span class="Cinzel">Winter</span>- 冬 -</p>
            <p class="p-image"><img class="is-imgChange" src="/season/common/img/img_block11_1_pc.png" alt="WINTER 冬"></p>
            <div class="l-block02-table">
                <div class="l-inner1160">
                    <div class="l-image">
                        <img src="/season/common/img/img_block11_2_pc.png" alt="凛とした空気に冬紅葉、結びの冬">
                    </div><div class="l-message">
                        <h3><img class="is-imgChange" src="/season/common/img/h3_block11_pc.png" alt="WINTER 冬"><span>- 冬 -</span></h3>
                        <div>
                            <p class="p-title">凛とした空気に冬紅葉、結びの冬</p>
                            <p class="p-message">雪化粧の表情や冬に赤く色づくもみじたち<br>2人の物語がここで今日結ばれる</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="l-block03 l-winter">
            <div class="l-block03-table">
                <div class="l-inner1160">
                	<div class="l-message">
                        <h4><img class="is-imgChange" src="/season/common/img/h4_block12_pc.png" alt="CUISINE 料理"><span><span class="Cinzel">Cuisine</span>料理</span></h4>
                        <div>
                            <p class="p-title">想いがこもる ぬくもりの冬</p>
                            <p class="p-message">蒸気の出るあたたかなお皿<br>温かいおもてなしの感動を胸に分かち合って</p>
                        	<p class="p-btn pconly2"><a class="Cinzel" href="/cuisine/">料理を見る</a></p>
                        </div>
                    </div><div class="l-image">
                        <img src="/season/common/img/img_block12_1_pc.png" alt="想いがこもる ぬくもりの冬"><br class="sponly1000">
                        <p class="p-btn sponly1000"><a class="Cinzel" href="/cuisine/">料理を見る</a></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="l-block04 l-winter">
        	<div class="l-inner1160">  	
            	<div class="l-block04-01">
                	<h4><span class="Cinzel">Winter Style</span>スタイル</h4>
                    <div class="l-block04-table">
                        <div>
                            <p class="p-image"><img src="/season/common/img/img_block13_1_pc.png" alt="冬のコーディネイト"></p>
                            <p class="p-message">
                                冬のコーディネイト
                            </p>
                        </div><div>
                            <p class="p-image"><img src="/season/common/img/img_block13_2_pc.png" alt="冬のキャンドル演出"></p>
                            <p class="p-message">
                                冬のキャンドル演出
                            </p>
                        </div><div>
                            <p class="p-image"><img src="/season/common/img/img_block13_3_pc.png" alt="冬ブーケ"></p>
                            <p class="p-message">
                                冬ブーケ
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php include("../common/inc/footer.php"); ?>
</body>
</html>
