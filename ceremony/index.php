<!doctype html>
<html lang="ja">
<head>
<?php include("../common/inc/head.php"); ?>
<title>Ceremony-挙式会場 | kitano garden</title>
<!-- ▼個別CSS▼ -->
<link rel="stylesheet" type="text/css" href="/ceremony/common/styles/ceremony.css">
<!-- ▲個別CSS▲ -->
<!-- ▼個別JS▼ -->
<script type="text/javascript" src="/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="/ceremony/common/js/ceremony.js"></script>
<!-- <script type="text/javascript" src="js/common/jquery.matchHeight.js"></script> -->
<!-- ▲個別JS▲ -->
</head>
<body>
<?php include("../common/inc/header.php"); ?>
<div id="wrapper">
    
    <div class="l-mv-line">
        <div class="l-mv-line-inner">
            <h2 class="l-mv-line-en">Ceremony</h2>
            <p class="l-mv-line-jp">挙式会場</p>
        </div>
    </div>
    <!-- / .l-mv -->
    
    <div class="l-mv-under">
        <h3 class="l-mv-under-ttl">選べる２つの挙式</h3>
        <ul class="l-mv-under-nav">
            <li><a href="#01" class="l-mv-under-nav-link01 is-pagescroll">森のチャペルで挙式</a></li>
            <li><a href="#02" class="l-mv-under-nav-link02 is-pagescroll">ガーデンで挙式</a></li>
        </ul>
    </div>
    <!-- / .l-mv-under -->
    
    <section id="01" class="l-block01 l-image-slide">
        <div class="l-image-slide-mv">
            <div class="l-image-slide-mv-img"><img class="is-imgChange" src="/ceremony/common/img/img_block01_pc.jpg" alt=""></div>
            <h3 class="l-image-slide-mv-ttl"><span class="l-image-slide-mv-ttl-en">Chapel</span><span class="l-image-slide-mv-ttl-jp">森のチャペル</span></h3>
        </div>
        <div class="l-image-slide-under">
            <h4 class="l-image-slide-under-ttl">information</h4>
            <div class="l-image-slide-under-spec">
                <table>
                    <tbody>
                        <tr>
                            <th>収容人数</th>
                            <td>最大90名</td>
                        </tr>
                        <tr>
                            <th>バージンロード</th>
                            <td>約10m</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="l-image-slide-photo-wrap">
            <ul class="l-image-slide-photo" id="slider01">
                <li><img src="/ceremony/common/img/img_block01_01.jpg" alt=""></li>
                <li><img src="/ceremony/common/img/img_block01_02.jpg" alt=""></li>
                <li><img src="/ceremony/common/img/img_block01_03.jpg" alt=""></li>
                <li><img src="/ceremony/common/img/img_block01_04.jpg" alt=""></li>
                <li><img src="/ceremony/common/img/img_block01_05.jpg" alt=""></li>
                <li><img src="/ceremony/common/img/img_block01_06.jpg" alt=""></li>
                <li><img src="/ceremony/common/img/img_block01_07.jpg" alt=""></li>
                <li><img src="/ceremony/common/img/img_block01_08.jpg" alt=""></li>
                <li><img src="/ceremony/common/img/img_block01_09.jpg" alt=""></li>
                <li><img src="/ceremony/common/img/img_block01_10.jpg" alt=""></li>
            </ul>
        </div>
        
    </section>
    <!-- / .l-block01 -->
    
    <section id="02" class="l-block02 l-image-slide">
        <div class="l-image-slide-mv">
            <div class="l-image-slide-mv-img"><img class="is-imgChange" src="/ceremony/common/img/img_block02_pc.jpg" alt=""></div>
            <h3 class="l-image-slide-mv-ttl"><span class="l-image-slide-mv-ttl-en">Garden</span><span class="l-image-slide-mv-ttl-jp">ガーデン挙式</span></h3>
        </div>
        <div class="l-image-slide-under">
            <h4 class="l-image-slide-under-ttl">information</h4>
            <div class="l-image-slide-under-spec">
                <table>
                    <tbody>
                        <tr>
                            <th>収容人数</th>
                            <td>最大90名</td>
                        </tr>
                        <tr>
                            <th>バージンロード</th>
                            <td>約10m</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="l-image-slide-photo-wrap">
            <ul class="l-image-slide-photo" id="slider02">
                <li><img src="/ceremony/common/img/img_block02_01.jpg" alt=""></li>
                <li><img src="/ceremony/common/img/img_block02_02.jpg" alt=""></li>
                <li><img src="/ceremony/common/img/img_block02_03.jpg" alt=""></li>
                <li><img src="/ceremony/common/img/img_block02_04.jpg" alt=""></li>
                <li><img src="/ceremony/common/img/img_block02_05.jpg" alt=""></li>
                <li><img src="/ceremony/common/img/img_block02_06.jpg" alt=""></li>
                <li><img src="/ceremony/common/img/img_block02_07.jpg" alt=""></li>
                <li><img src="/ceremony/common/img/img_block02_08.jpg" alt=""></li>
                <li><img src="/ceremony/common/img/img_block02_09.jpg" alt=""></li>
                <li><img src="/ceremony/common/img/img_block02_10.jpg" alt=""></li>
            </ul>
        </div>
        
    </section>
    <!-- / .l-block01 -->
    <?php include("../common/inc/pickupfair.php"); ?>

    
</div>
<!-- / #wrapper -->
<?php include("../common/inc/footer.php"); ?>
</body>
</html>
