<!doctype html>
<html lang="ja">
<head>
<?php include("../common/inc/head.php"); ?>
<title>WEDDING PLAN プラン｜kitano garden</title>
<!-- ▼個別CSS▼ -->
<link rel="stylesheet" type="text/css" href="/plan/common/styles/plan.css">
<link rel="stylesheet" type="text/css" href="/common/styles/lightbox/lightbox.css">
<!-- ▲個別CSS▲ -->
<!-- ▼個別JS▼ -->
<script type="text/javascript" src="/plan/common/js/plan.js"></script>
<script type="text/javascript" src="/common/js/jquery.matchHeight/jquery.matchHeight.js"></script>
<script type="text/javascript" src="/common/js/lightbox/lightbox.js"></script>
<!-- ▲個別JS▲ -->
</head>
<body>
<?php include("../common/inc/header.php"); ?>
<div id="wrapper">
    <section>
    <div class="l-block01">
    	<div class="l-inner">
            <h2><img class="is-imgChange" src="/plan/common/img/h2_block01_pc.png" alt="WEDDING PLAN ウェディングプラン"><span><span class="Cinzel">Wedding Plan</span>ウェディングプラン</span></h2>
        </div>
    </div>
    </section>
        
    <section>
    <div class="l-block02 p-type01">
    	<div class="l-inner">
            <div id="01" class="l-block02-table">
            	<div><p class="p-kikan">10ヶ月以内 / 挙式 + 披露宴</p></div>
                <div><p class="p-title">期間限定スペシャル40</p></div>
                <div><p class="p-price">40名<span class="Cinzel">&yen;1,600,000-</span></p></div>
                <div><p class="p-tsuika">[1名様追加¥18,150-]</p></div>
                <div class="l-btn js-menu"><p>プラン内容</p></div>
                <div class="l-detail">
                	<div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_01.png" alt="フランス料理12,000円"></p>
                        <p class="p-message">フランス料理<br>12,000円</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_02.png" alt="ウェルカムドリンク"></p>
                        <p class="p-message">ウェルカムドリンク</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_03.png" alt="乾杯シャンパン"></p>
                        <p class="p-message">乾杯シャンパン</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_04.png" alt="パーティドリンク"></p>
                        <p class="p-message">パーティドリンク</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_05.png" alt="ウェディングケーキ"></p>
                        <p class="p-message">ウェディングケーキ</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_06.png" alt="メインテーブル装花"></p>
                        <p class="p-message">メインテーブル<br>装花</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_07.png" alt="ゲストテーブル装花"></p>
                        <p class="p-message">ゲストテーブル<br>装花</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_08.png" alt="オーダーメイドブーケ&ブートニア"></p>
                        <p class="p-message">オーダーメイド<br>ブーケ&amp;ブートニア</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_09.png" alt="挙式料（人前式）"></p>
                        <p class="p-message">挙式料<br>（人前式）</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_10.png" alt="挙式司会者"></p>
                        <p class="p-message">挙式司会者</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_11.png" alt="ウェディングドレス&ドレス小物"></p>
                        <p class="p-message">ウェディングドレス<br>&amp;ドレス小物</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_12.png" alt="タキシード&小物"></p>
                        <p class="p-message">タキシード<br>&amp;小物</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_13.png" alt="新婦ヘアメイク&着付け"></p>
                        <p class="p-message">新婦ヘアメイク<br>&amp;着付け</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_14.png" alt="新郎着付け"></p>
                        <p class="p-message">新郎着付け</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_15.png" alt="記念写真"></p>
                        <p class="p-message">記念写真</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_16.png" alt="引出物"></p>
                        <p class="p-message">引出物</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_17.png" alt="引菓子"></p>
                        <p class="p-message">引菓子</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_18.png" alt="ペーパーバッグ"></p>
                        <p class="p-message">ペーパーバッグ</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_19.png" alt="招待状"></p>
                        <p class="p-message">招待状</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_20.png" alt="席札&メニュー"></p>
                        <p class="p-message">席札<br>&amp;メニュー</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_21.png" alt="パーティ司会者"></p>
                        <p class="p-message">パーティ司会者</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_22.png" alt="会場料"></p>
                        <p class="p-message">会場料</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_23.png" alt="控室料"></p>
                        <p class="p-message">控室料</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_24.png" alt="音響照明使用料"></p>
                        <p class="p-message">音響照明使用料</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_25.png" alt="新婦当日アテンド"></p>
                        <p class="p-message">新婦当日アテンド</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_26.png" alt="当日キャプテン"></p>
                        <p class="p-message">当日キャプテン</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_27.png" alt="サービス料"></p>
                        <p class="p-message">サービス料</p>
                    </div>
                </div>
            </div>
            
            <div id="02" class="l-block02-table">
            	<div><p class="p-kikan">10ヶ月以内 / 挙式 + 披露宴</p></div>
                <div><p class="p-title">期間限定スペシャル40<br>お色直しつき</p></div>
                <div><p class="p-price">40名<span class="Cinzel">&yen;1,720,000-</span></p></div>
                <div><p class="p-tsuika">[1名様追加¥18,150-]</p></div>
                <div class="l-btn js-menu"><p>プラン内容</p></div>
                <div class="l-detail">
                	<div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_01.png" alt="フランス料理12,000円"></p>
                        <p class="p-message">フランス料理<br>12,000円</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_02.png" alt="ウェルカムドリンク"></p>
                        <p class="p-message">ウェルカムドリンク</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_03.png" alt="乾杯シャンパン"></p>
                        <p class="p-message">乾杯シャンパン</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_04.png" alt="パーティドリンク"></p>
                        <p class="p-message">パーティドリンク</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_05.png" alt="ウェディングケーキ"></p>
                        <p class="p-message">ウェディングケーキ</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_06.png" alt="メインテーブル装花"></p>
                        <p class="p-message">メインテーブル<br>装花</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_07.png" alt="ゲストテーブル装花"></p>
                        <p class="p-message">ゲストテーブル<br>装花</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_08.png" alt="オーダーメイドブーケ&ブートニア"></p>
                        <p class="p-message">オーダーメイド<br>ブーケ&amp;ブートニア</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_09.png" alt="挙式料（人前式）"></p>
                        <p class="p-message">挙式料<br>（人前式）</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_10.png" alt="挙式司会者"></p>
                        <p class="p-message">挙式司会者</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_11.png" alt="ウェディングドレス&ドレス小物"></p>
                        <p class="p-message">ウェディングドレス<br>&amp;ドレス小物</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_28.png" alt="カラードレス&ドレス小物"></p>
                        <p class="p-message">カラードレス<br>&amp;ドレス小物</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_12.png" alt="タキシード&小物"></p>
                        <p class="p-message">タキシード<br>&amp;小物</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_13.png" alt="新婦ヘアメイク&着付け(2点分)"></p>
                        <p class="p-message">新婦ヘアメイク<br>&amp;着付け(2点分)</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_14.png" alt="新郎着付け"></p>
                        <p class="p-message">新郎着付け</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_15.png" alt="記念写真"></p>
                        <p class="p-message">記念写真</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_16.png" alt="引出物"></p>
                        <p class="p-message">引出物</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_17.png" alt="引菓子"></p>
                        <p class="p-message">引菓子</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_18.png" alt="ペーパーバッグ"></p>
                        <p class="p-message">ペーパーバッグ</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_19.png" alt="招待状"></p>
                        <p class="p-message">招待状</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_20.png" alt="席札&メニュー"></p>
                        <p class="p-message">席札<br>&amp;メニュー</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_21.png" alt="パーティ司会者"></p>
                        <p class="p-message">パーティ司会者</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_22.png" alt="会場料"></p>
                        <p class="p-message">会場料</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_23.png" alt="控室料"></p>
                        <p class="p-message">控室料</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_24.png" alt="音響照明使用料"></p>
                        <p class="p-message">音響照明使用料</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_25.png" alt="新婦当日アテンド"></p>
                        <p class="p-message">新婦当日アテンド</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_26.png" alt="当日キャプテン"></p>
                        <p class="p-message">当日キャプテン</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_27.png" alt="サービス料"></p>
                        <p class="p-message">サービス料</p>
                    </div>
                </div>
            </div>
            
            <div id="03" class="l-block02-table">
            	<div><p class="p-kikan">10ヶ月以内 / 挙式 + 披露宴</p></div>
                <div><p class="p-title">期間限定スペシャル40<br>アルバム&amp;エンドロールつき</p></div>
                <div><p class="p-price">40名<span class="Cinzel">&yen;2,000,000-</span></p></div>
                <div><p class="p-tsuika">[1名様追加¥18,150-]</p></div>
                <div class="l-btn js-menu"><p>プラン内容</p></div>
                <div class="l-detail">
                	<div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_01.png" alt="フランス料理12,000円"></p>
                        <p class="p-message">フランス料理<br>12,000円</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_02.png" alt="ウェルカムドリンク"></p>
                        <p class="p-message">ウェルカムドリンク</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_03.png" alt="乾杯シャンパン"></p>
                        <p class="p-message">乾杯シャンパン</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_04.png" alt="パーティドリンク"></p>
                        <p class="p-message">パーティドリンク</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_05.png" alt="ウェディングケーキ"></p>
                        <p class="p-message">ウェディングケーキ</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_06.png" alt="メインテーブル装花"></p>
                        <p class="p-message">メインテーブル<br>装花</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_07.png" alt="ゲストテーブル装花"></p>
                        <p class="p-message">ゲストテーブル<br>装花</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_08.png" alt="オーダーメイドブーケ&ブートニア"></p>
                        <p class="p-message">オーダーメイド<br>ブーケ&amp;ブートニア</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_09.png" alt="挙式料（人前式）"></p>
                        <p class="p-message">挙式料<br>（人前式）</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_10.png" alt="挙式司会者"></p>
                        <p class="p-message">挙式司会者</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_11.png" alt="ウェディングドレス&ドレス小物"></p>
                        <p class="p-message">ウェディングドレス<br>&amp;ドレス小物</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_12.png" alt="タキシード&小物"></p>
                        <p class="p-message">タキシード<br>&amp;小物</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_13.png" alt="新婦ヘアメイク&着付け"></p>
                        <p class="p-message">新婦ヘアメイク<br>&amp;着付け</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_14.png" alt="新郎着付け"></p>
                        <p class="p-message">新郎着付け</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_15.png" alt="記念写真"></p>
                        <p class="p-message">記念写真</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_16.png" alt="引出物"></p>
                        <p class="p-message">引出物</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_17.png" alt="引菓子"></p>
                        <p class="p-message">引菓子</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_18.png" alt="ペーパーバッグ"></p>
                        <p class="p-message">ペーパーバッグ</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_19.png" alt="招待状"></p>
                        <p class="p-message">招待状</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_20.png" alt="席札&メニュー"></p>
                        <p class="p-message">席札<br>&amp;メニュー</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_29.png" alt="アルバム1冊&データ○○カット付"></p>
                        <p class="p-message">アルバム1冊<br>&amp;データ○○カット付</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_30.png" alt="ダイジェストエンドロール"></p>
                        <p class="p-message">ダイジェストエンドロール</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_31.png" alt="スクリーン・プロジェクター使用料"></p>
                        <p class="p-message">スクリーン・<br>プロジェクター使用料</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_21.png" alt="パーティ司会者"></p>
                        <p class="p-message">パーティ司会者</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_22.png" alt="会場料"></p>
                        <p class="p-message">会場料</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_23.png" alt="控室料"></p>
                        <p class="p-message">控室料</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_24.png" alt="音響照明使用料"></p>
                        <p class="p-message">音響照明使用料</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_25.png" alt="新婦当日アテンド"></p>
                        <p class="p-message">新婦当日アテンド</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_26.png" alt="当日キャプテン"></p>
                        <p class="p-message">当日キャプテン</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_27.png" alt="サービス料"></p>
                        <p class="p-message">サービス料</p>
                    </div>
                </div>
            </div>
            
            <div id="04" class="l-block02-table">
            	<div><p class="p-kikan">10ヶ月以内 / 挙式 + 披露宴</p></div>
                <div><p class="p-title">期間限定スペシャル60</p></div>
                <div><p class="p-price">60名<span class="Cinzel">&yen;2,000,000-</span></p></div>
                <div><p class="p-tsuika">[1名様追加¥18,150-]</p></div>
                <div class="l-btn js-menu"><p>プラン内容</p></div>
                <div class="l-detail">
                	<div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_01.png" alt="フランス料理12,000円"></p>
                        <p class="p-message">フランス料理<br>12,000円</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_02.png" alt="ウェルカムドリンク"></p>
                        <p class="p-message">ウェルカムドリンク</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_03.png" alt="乾杯シャンパン"></p>
                        <p class="p-message">乾杯シャンパン</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_04.png" alt="パーティドリンク"></p>
                        <p class="p-message">パーティドリンク</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_05.png" alt="ウェディングケーキ"></p>
                        <p class="p-message">ウェディングケーキ</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_06.png" alt="メインテーブル装花"></p>
                        <p class="p-message">メインテーブル<br>装花</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_07.png" alt="ゲストテーブル装花"></p>
                        <p class="p-message">ゲストテーブル<br>装花</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_08.png" alt="オーダーメイドブーケ&ブートニア"></p>
                        <p class="p-message">オーダーメイド<br>ブーケ&amp;ブートニア</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_09.png" alt="挙式料（人前式）"></p>
                        <p class="p-message">挙式料<br>（人前式）</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_10.png" alt="挙式司会者"></p>
                        <p class="p-message">挙式司会者</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_11.png" alt="ウェディングドレス&ドレス小物"></p>
                        <p class="p-message">ウェディングドレス<br>&amp;ドレス小物</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_12.png" alt="タキシード&小物"></p>
                        <p class="p-message">タキシード<br>&amp;小物</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_13.png" alt="新婦ヘアメイク&着付け"></p>
                        <p class="p-message">新婦ヘアメイク<br>&amp;着付け</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_14.png" alt="新郎着付け"></p>
                        <p class="p-message">新郎着付け</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_15.png" alt="記念写真"></p>
                        <p class="p-message">記念写真</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_16.png" alt="引出物"></p>
                        <p class="p-message">引出物</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_17.png" alt="引菓子"></p>
                        <p class="p-message">引菓子</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_18.png" alt="ペーパーバッグ"></p>
                        <p class="p-message">ペーパーバッグ</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_19.png" alt="招待状"></p>
                        <p class="p-message">招待状</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_20.png" alt="席札&メニュー"></p>
                        <p class="p-message">席札<br>&amp;メニュー</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_21.png" alt="パーティ司会者"></p>
                        <p class="p-message">パーティ司会者</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_22.png" alt="会場料"></p>
                        <p class="p-message">会場料</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_23.png" alt="控室料"></p>
                        <p class="p-message">控室料</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_24.png" alt="音響照明使用料"></p>
                        <p class="p-message">音響照明使用料</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_25.png" alt="新婦当日アテンド"></p>
                        <p class="p-message">新婦当日アテンド</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_26.png" alt="当日キャプテン"></p>
                        <p class="p-message">当日キャプテン</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_27.png" alt="サービス料"></p>
                        <p class="p-message">サービス料</p>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    
    <div class="l-block02 p-type02">
    	<div class="l-inner">        
            <div id="05" class="l-block02-table">
            	<div><p class="p-kikan">通年OK / 挙式 + 披露宴</p></div>
                <div><p class="p-title"><span>プレ花嫁様向け</span>結婚準備応援</p></div>
                <div><p class="p-price">60名<span class="Cinzel">&yen;2,250,000-</span></p></div>
                <div><p class="p-tsuika">[1名様追加¥18,150-]</p></div>
                <div class="l-btn js-menu"><p>プラン内容</p></div>
                <div class="l-detail">
                	<div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_01.png" alt="フランス料理12,000円"></p>
                        <p class="p-message">フランス料理<br>12,000円</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_02.png" alt="ウェルカムドリンク"></p>
                        <p class="p-message">ウェルカムドリンク</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_03.png" alt="乾杯シャンパン"></p>
                        <p class="p-message">乾杯シャンパン</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_04.png" alt="パーティドリンク"></p>
                        <p class="p-message">パーティドリンク</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_05.png" alt="ウェディングケーキ"></p>
                        <p class="p-message">ウェディングケーキ</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_06.png" alt="メインテーブル装花"></p>
                        <p class="p-message">メインテーブル<br>装花</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_07.png" alt="ゲストテーブル装花"></p>
                        <p class="p-message">ゲストテーブル<br>装花</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_08.png" alt="オーダーメイドブーケ&ブートニア"></p>
                        <p class="p-message">オーダーメイド<br>ブーケ&amp;ブートニア</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_09.png" alt="挙式料（人前式）"></p>
                        <p class="p-message">挙式料<br>（人前式）</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_10.png" alt="挙式司会者"></p>
                        <p class="p-message">挙式司会者</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_11.png" alt="ウェディングドレス&ドレス小物"></p>
                        <p class="p-message">ウェディングドレス<br>&amp;ドレス小物</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_12.png" alt="タキシード&小物"></p>
                        <p class="p-message">タキシード<br>&amp;小物</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_13.png" alt="新婦ヘアメイク&着付け"></p>
                        <p class="p-message">新婦ヘアメイク<br>&amp;着付け</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_14.png" alt="新郎着付け"></p>
                        <p class="p-message">新郎着付け</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_15.png" alt="記念写真"></p>
                        <p class="p-message">記念写真</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_16.png" alt="引出物"></p>
                        <p class="p-message">引出物</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_17.png" alt="引菓子"></p>
                        <p class="p-message">引菓子</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_18.png" alt="ペーパーバッグ"></p>
                        <p class="p-message">ペーパーバッグ</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_19.png" alt="招待状"></p>
                        <p class="p-message">招待状</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_20.png" alt="席札&メニュー"></p>
                        <p class="p-message">席札<br>&amp;メニュー</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_21.png" alt="パーティ司会者"></p>
                        <p class="p-message">パーティ司会者</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_22.png" alt="会場料"></p>
                        <p class="p-message">会場料</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_23.png" alt="控室料"></p>
                        <p class="p-message">控室料</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_24.png" alt="音響照明使用料"></p>
                        <p class="p-message">音響照明使用料</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_25.png" alt="新婦当日アテンド"></p>
                        <p class="p-message">新婦当日アテンド</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_26.png" alt="当日キャプテン"></p>
                        <p class="p-message">当日キャプテン</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_27.png" alt="サービス料"></p>
                        <p class="p-message">サービス料</p>
                    </div>
                </div>
            </div>
            <div id="06" class="l-block02-table">
            	<div><p class="p-kikan">通年OK / 挙式 + 披露宴</p></div>
                <div><p class="p-title"><span>お料理重視の方へ</span>おもてなし美食</p></div>
                <div><p class="p-price">40名<span class="Cinzel">&yen;1,800,000-</span></p></div>
                <div><p class="p-tsuika">[1名様追加¥23,100-]</p></div>
                <div class="l-btn js-menu"><p>プラン内容</p></div>
                <div class="l-detail">
                	<div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_33.png" alt="フランス料理15,000円"></p>
                        <p class="p-message">フランス料理<br>15,000円</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_02.png" alt="ウェルカムドリンク"></p>
                        <p class="p-message">ウェルカムドリンク</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_03.png" alt="乾杯シャンパン"></p>
                        <p class="p-message">乾杯シャンパン</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_04.png" alt="パーティドリンク"></p>
                        <p class="p-message">パーティドリンク</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_05.png" alt="ウェディングケーキ"></p>
                        <p class="p-message">ウェディングケーキ</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_06.png" alt="メインテーブル装花"></p>
                        <p class="p-message">メインテーブル<br>装花</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_07.png" alt="ゲストテーブル装花"></p>
                        <p class="p-message">ゲストテーブル<br>装花</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_08.png" alt="オーダーメイドブーケ&ブートニア"></p>
                        <p class="p-message">オーダーメイド<br>ブーケ&amp;ブートニア</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_09.png" alt="挙式料（人前式）"></p>
                        <p class="p-message">挙式料<br>（人前式）</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_10.png" alt="挙式司会者"></p>
                        <p class="p-message">挙式司会者</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_11.png" alt="ウェディングドレス&ドレス小物"></p>
                        <p class="p-message">ウェディングドレス<br>&amp;ドレス小物</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_12.png" alt="タキシード&小物"></p>
                        <p class="p-message">タキシード<br>&amp;小物</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_13.png" alt="新婦ヘアメイク&着付け"></p>
                        <p class="p-message">新婦ヘアメイク<br>&amp;着付け</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_14.png" alt="新郎着付け"></p>
                        <p class="p-message">新郎着付け</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_15.png" alt="記念写真"></p>
                        <p class="p-message">記念写真</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_16.png" alt="引出物"></p>
                        <p class="p-message">引出物</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_17.png" alt="引菓子"></p>
                        <p class="p-message">引菓子</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_18.png" alt="ペーパーバッグ"></p>
                        <p class="p-message">ペーパーバッグ</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_19.png" alt="招待状"></p>
                        <p class="p-message">招待状</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_20.png" alt="席札&メニュー"></p>
                        <p class="p-message">席札<br>&amp;メニュー</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_21.png" alt="パーティ司会者"></p>
                        <p class="p-message">パーティ司会者</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_22.png" alt="会場料"></p>
                        <p class="p-message">会場料</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_23.png" alt="控室料"></p>
                        <p class="p-message">控室料</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_24.png" alt="音響照明使用料"></p>
                        <p class="p-message">音響照明使用料</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_25.png" alt="新婦当日アテンド"></p>
                        <p class="p-message">新婦当日アテンド</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_26.png" alt="当日キャプテン"></p>
                        <p class="p-message">当日キャプテン</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_27.png" alt="サービス料"></p>
                        <p class="p-message">サービス料</p>
                    </div>
                </div>
            </div>
            <div id="07" class="l-block02-table">
            	<div><p class="p-kikan">12ヶ月以内 / 披露宴のみ</p></div>
                <div><p class="p-title">おひろめ披露宴</p></div>
                <div><p class="p-price">40名<span class="Cinzel">&yen;1,500,000-</span></p></div>
                <div><p class="p-tsuika">[1名様追加¥18,150-]</p></div>
                <div class="l-btn js-menu"><p>プラン内容</p></div>
                <div class="l-detail">
                	<div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_01.png" alt="フランス料理12,000円"></p>
                        <p class="p-message">フランス料理<br>12,000円</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_02.png" alt="ウェルカムドリンク"></p>
                        <p class="p-message">ウェルカムドリンク</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_03.png" alt="乾杯シャンパン"></p>
                        <p class="p-message">乾杯シャンパン</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_04.png" alt="パーティドリンク"></p>
                        <p class="p-message">パーティドリンク</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_05.png" alt="ウェディングケーキ"></p>
                        <p class="p-message">ウェディングケーキ</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_06.png" alt="メインテーブル装花"></p>
                        <p class="p-message">メインテーブル<br>装花</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_07.png" alt="ゲストテーブル装花"></p>
                        <p class="p-message">ゲストテーブル<br>装花</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_08.png" alt="オーダーメイドブーケ&ブートニア"></p>
                        <p class="p-message">オーダーメイド<br>ブーケ&amp;ブートニア</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_11.png" alt="ウェディングドレス&ドレス小物"></p>
                        <p class="p-message">ウェディングドレス<br>&amp;ドレス小物</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_12.png" alt="タキシード&小物"></p>
                        <p class="p-message">タキシード<br>&amp;小物</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_13.png" alt="新婦ヘアメイク&着付け"></p>
                        <p class="p-message">新婦ヘアメイク<br>&amp;着付け</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_14.png" alt="新郎着付け"></p>
                        <p class="p-message">新郎着付け</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_15.png" alt="記念写真"></p>
                        <p class="p-message">記念写真</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_16.png" alt="引出物"></p>
                        <p class="p-message">引出物</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_17.png" alt="引菓子"></p>
                        <p class="p-message">引菓子</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_18.png" alt="ペーパーバッグ"></p>
                        <p class="p-message">ペーパーバッグ</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_19.png" alt="招待状"></p>
                        <p class="p-message">招待状</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_20.png" alt="席札&メニュー"></p>
                        <p class="p-message">席札<br>&amp;メニュー</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_21.png" alt="パーティ司会者"></p>
                        <p class="p-message">パーティ司会者</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_22.png" alt="会場料"></p>
                        <p class="p-message">会場料</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_23.png" alt="控室料"></p>
                        <p class="p-message">控室料</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_24.png" alt="音響照明使用料"></p>
                        <p class="p-message">音響照明使用料</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_25.png" alt="新婦当日アテンド"></p>
                        <p class="p-message">新婦当日アテンド</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_26.png" alt="当日キャプテン"></p>
                        <p class="p-message">当日キャプテン</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_27.png" alt="サービス料"></p>
                        <p class="p-message">サービス料</p>
                    </div>
                </div>
            </div>
    	</div>
    </div>
    
    <div class="l-block02 p-type03">
    	<div class="l-inner">        
            <div id="08" class="l-block02-table">
            	<div><p class="p-kikan">4ヶ月以内 / 挙式 + 披露宴</p></div>
                <div><p class="p-title"><span>少人数</span>挙式・会食</p></div>
                <div><p class="p-price">20名<span class="Cinzel">&yen;1,150,000-</span></p></div>
                <div><p class="p-tsuika">[1名様追加¥18,150-]</p></div>
                <div class="l-btn js-menu"><p>プラン内容</p></div>
                <div class="l-detail">
                	<div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_01.png" alt="フランス料理12,000円"></p>
                        <p class="p-message">フランス料理<br>12,000円</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_02.png" alt="ウェルカムドリンク"></p>
                        <p class="p-message">ウェルカムドリンク</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_03.png" alt="乾杯シャンパン"></p>
                        <p class="p-message">乾杯シャンパン</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_04.png" alt="パーティドリンク"></p>
                        <p class="p-message">パーティドリンク</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_05.png" alt="ウェディングケーキ"></p>
                        <p class="p-message">ウェディングケーキ</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_06.png" alt="メインテーブル装花"></p>
                        <p class="p-message">メインテーブル<br>装花</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_07.png" alt="ゲストテーブル装花"></p>
                        <p class="p-message">ゲストテーブル<br>装花</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_08.png" alt="オーダーメイドブーケ&ブートニア"></p>
                        <p class="p-message">オーダーメイド<br>ブーケ&amp;ブートニア</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_09.png" alt="挙式料（人前式）"></p>
                        <p class="p-message">挙式料<br>（人前式）</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_10.png" alt="挙式司会者"></p>
                        <p class="p-message">挙式司会者</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_11.png" alt="ウェディングドレス&ドレス小物"></p>
                        <p class="p-message">ウェディングドレス<br>&amp;ドレス小物</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_12.png" alt="タキシード&小物"></p>
                        <p class="p-message">タキシード<br>&amp;小物</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_13.png" alt="新婦ヘアメイク&着付け"></p>
                        <p class="p-message">新婦ヘアメイク<br>&amp;着付け</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_14.png" alt="新郎着付け"></p>
                        <p class="p-message">新郎着付け</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_15.png" alt="記念写真"></p>
                        <p class="p-message">記念写真</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_21.png" alt="パーティ司会者"></p>
                        <p class="p-message">パーティ司会者</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_22.png" alt="会場料"></p>
                        <p class="p-message">会場料</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_23.png" alt="控室料"></p>
                        <p class="p-message">控室料</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_24.png" alt="音響照明使用料"></p>
                        <p class="p-message">音響照明使用料</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_25.png" alt="新婦当日アテンド"></p>
                        <p class="p-message">新婦当日アテンド</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_26.png" alt="当日キャプテン"></p>
                        <p class="p-message">当日キャプテン</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_27.png" alt="サービス料"></p>
                        <p class="p-message">サービス料</p>
                    </div>
                </div>
            </div>
            
            <div id="09" class="l-block02-table">
            	<div><p class="p-kikan">3ヶ月以内 / お食事 + フォト</p></div>
                <div><p class="p-title"><span>親族中心で</span>お食事会スタイル</p></div>
                <div><p class="p-price">20名<span class="Cinzel">&yen;600,000-</span></p></div>
                <div><p class="p-tsuika">[1名様追加¥17,050-]</p></div>
                <div class="l-btn js-menu"><p>プラン内容</p></div>
                <div class="l-detail">
                	<div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_01.png" alt="フランス料理12,000円"></p>
                        <p class="p-message">フランス料理<br>12,000円</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_32.png" alt="乾杯スパークリング"></p>
                        <p class="p-message">乾杯スパークリング</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_04.png" alt="パーティドリンク"></p>
                        <p class="p-message">パーティドリンク</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_05.png" alt="ウェディングケーキ"></p>
                        <p class="p-message">ウェディングケーキ</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_06.png" alt="メインテーブル装花"></p>
                        <p class="p-message">メインテーブル<br>装花</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_07.png" alt="ゲストテーブル装花"></p>
                        <p class="p-message">ゲストテーブル<br>装花</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_15.png" alt="記念写真"></p>
                        <p class="p-message">記念写真</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_21.png" alt="パーティ司会者"></p>
                        <p class="p-message">パーティ司会者</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_22.png" alt="会場料"></p>
                        <p class="p-message">会場料</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_23.png" alt="控室料"></p>
                        <p class="p-message">控室料</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_24.png" alt="音響照明使用料"></p>
                        <p class="p-message">音響照明使用料</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_25.png" alt="新婦当日アテンド"></p>
                        <p class="p-message">新婦当日アテンド</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_26.png" alt="当日キャプテン"></p>
                        <p class="p-message">当日キャプテン</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_27.png" alt="サービス料"></p>
                        <p class="p-message">サービス料</p>
                    </div>
                </div>
            </div>
            
            <div id="10" class="l-block02-table">
            	<div><p class="p-kikan">4ヶ月以内 / 挙式 + 披露宴</p></div>
                <div><p class="p-title"><span>“ダブルハッピーで幸せ２倍”</span>スウィートマタニティ</p></div>
                <div><p class="p-price">30名<span class="Cinzel">&yen;1,300,000-</span></p></div>
                <div><p class="p-tsuika">[1名様追加¥18,150-]</p></div>
                <div class="l-btn js-menu"><p>プラン内容</p></div>
                <div class="l-detail">
                	<div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_01.png" alt="フランス料理12,000円"></p>
                        <p class="p-message">フランス料理<br>12,000円</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_02.png" alt="ウェルカムドリンク"></p>
                        <p class="p-message">ウェルカムドリンク</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_03.png" alt="乾杯シャンパン"></p>
                        <p class="p-message">乾杯シャンパン</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_04.png" alt="パーティドリンク"></p>
                        <p class="p-message">パーティドリンク</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_05.png" alt="ウェディングケーキ"></p>
                        <p class="p-message">ウェディングケーキ</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_06.png" alt="メインテーブル装花"></p>
                        <p class="p-message">メインテーブル<br>装花</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_07.png" alt="ゲストテーブル装花"></p>
                        <p class="p-message">ゲストテーブル<br>装花</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_08.png" alt="オーダーメイドブーケ&ブートニア"></p>
                        <p class="p-message">オーダーメイド<br>ブーケ&amp;ブートニア</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_09.png" alt="挙式料（人前式）"></p>
                        <p class="p-message">挙式料<br>（人前式）</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_10.png" alt="挙式司会者"></p>
                        <p class="p-message">挙式司会者</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_11.png" alt="ウェディングドレス&ドレス小物"></p>
                        <p class="p-message">ウェディングドレス<br>&amp;ドレス小物</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_12.png" alt="タキシード&小物"></p>
                        <p class="p-message">タキシード<br>&amp;小物</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_13.png" alt="新婦ヘアメイク&着付け"></p>
                        <p class="p-message">新婦ヘアメイク<br>&amp;着付け</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_14.png" alt="新郎着付け"></p>
                        <p class="p-message">新郎着付け</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_15.png" alt="記念写真"></p>
                        <p class="p-message">記念写真</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_16.png" alt="引出物"></p>
                        <p class="p-message">引出物</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_17.png" alt="引菓子"></p>
                        <p class="p-message">引菓子</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_18.png" alt="ペーパーバッグ"></p>
                        <p class="p-message">ペーパーバッグ</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_19.png" alt="招待状"></p>
                        <p class="p-message">招待状</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_20.png" alt="席札&メニュー"></p>
                        <p class="p-message">席札<br>&amp;メニュー</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_21.png" alt="パーティ司会者"></p>
                        <p class="p-message">パーティ司会者</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_22.png" alt="会場料"></p>
                        <p class="p-message">会場料</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_23.png" alt="控室料"></p>
                        <p class="p-message">控室料</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_24.png" alt="音響照明使用料"></p>
                        <p class="p-message">音響照明使用料</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_25.png" alt="新婦当日アテンド"></p>
                        <p class="p-message">新婦当日アテンド</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_26.png" alt="当日キャプテン"></p>
                        <p class="p-message">当日キャプテン</p>
                    </div><div>
                    	<p class="p-image"><img src="/plan/common/img/img_block01_27.png" alt="サービス料"></p>
                        <p class="p-message">サービス料</p>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
            
    </section>
    
    <section>
    <div class="l-block03">
    	<div class="l-image">
        	<img src="/plan/common/img/img_block03_pc.png" alt="">
        </div><div class="l-message">
        	<p>
            	※挙式はガーデンまたはチャペルをお選び頂けます。<br>
                ※キリスト教式も可能です。（+50,000円）<br>
                ※価格は税別となります。<br>
                ※ご結婚式のご予約は先着順のため、仮予約はお受けしておりません。<br>
                あらかじめご了承下さいませ。
            </p>
        </div>
    </div>
    </section>
    <?php include("../common/inc/pickupfair.php"); ?>
</div>
<?php include("../common/inc/footer.php"); ?>
</body>
</html>
