<!doctype html>
<html lang="ja">
<head>
<?php include("../common/inc/head.php"); ?>
<title>FLOWER 装花｜kitano garden</title>
<!-- ▼個別CSS▼ -->
<link rel="stylesheet" type="text/css" href="/flower/common/styles/flower.css">
<!-- ▲個別CSS▲ -->
<!-- ▼個別JS▼ -->
<script type="text/javascript" src="/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="/flower/common/js/flower.js"></script>
<!-- ▲個別JS▲ -->
</head>
<body>
<?php include("../common/inc/header.php"); ?>
<div class="l-mvBlock">
    <div class="l-mv">
    	
    </div>
</div>    
<div id="wrapper">
    <section>
    <div class="l-block01">
    	<div class="l-inner">
            <h2><img class="is-imgChange" src="/flower/common/img/h2_block01_pc.png" alt="FLOWER 装花"><span><span class="Cinzel">Flower</span>装花</span></h2>
            <p class="p-title">北野ガーデンの装花を担当するのは、<br>関西を代表するフローリスト「jurret（ユーレット）」</p>
        	<p class="p-message">
				“その日、その空間をドラマティックに変える”<br>
                ここにしかないただ一つだけの世界を作り出す、<br>
                お花のアーティスト
            </p>
            <div class="l-block01-table">
            	<div>
                	<p class="p-image"><img src="/flower/common/img/img_block01_1_pc.png" alt=""></p>
                    <p class="p-message">
                    	その独創的な世界観とオリジナリティあふれる装花のコーディネートが人気。会場のテーブル装花コーディネートやガーデンの装飾、新婦様のブーケ、ウェルカムスペースの飾りつけやケーキのデコレーションまで・・・
                    </p>
                </div><div>
                	<p class="p-image"><img src="/flower/common/img/img_block01_2_pc.png" alt=""></p>
                    <p class="p-message">
                    	お二人のご希望をていねいにヒアリング。<br>
                        打合せでは通常のウェディングプランニングではめずらしく、お花の打合せを専属のフラワーデザイナーが2回させていただきます。
                    </p>
                </div><div>
                	<p class="p-image"><img src="/flower/common/img/img_block01_3_pc.png" alt=""></p>
                    <p class="p-message">
                    	北野ガーデンの会場全体の空間を最大限に活かしながらも、新郎新婦様の“好き”をいっぱい詰め込んだ、世界にひとつだけのオーダーメイドフラワーを叶えます。
                    </p>
                </div>
            </div>
        </div>
    </div>
    </section>
    
    <section>
    <div class="l-block02 l-block">
        <h3><span class="Cinzel">Gallery</span>装花ギャラリー</h3>
        <div class="l-block-photo-wrap">
            <ul class="l-block-photo" id="slider02">
                <li><p><img src="/flower/common/img/img_block02_1_pc.png" alt=""><br><span class="Gothic">１行説明テキストが入ります。１行説明テキストが入ります。</span></p></li>
                <li><p><img src="/flower/common/img/img_block02_2_pc.png" alt=""><br><span class="Gothic">１行説明テキストが入ります。</span></p></li>
                <li><p><img src="/flower/common/img/img_block02_3_pc.png" alt=""><br><span class="Gothic">１行説明テキストが入ります。</span></p></li>
                <li><p><img src="/flower/common/img/img_block02_4_pc.png" alt=""><br><span class="Gothic">１行説明テキストが入ります。</span></p></li>
            </ul>
        </div>
    </div>
    </section>
    
    <section>
    <div class="l-block03">
    	<div class="l-inner">
            <div class="l-image">
                <img src="/flower/common/img/img_block03_1_pc.png" alt="">
            </div><div class="l-message">
                <p class="p-title"><span class="Cinzel">Jurret</span>（ユーレット）</p>
                <p class="p-adress Gothic">
                	住所　神戸市中央区山本通2-14-18　2F<br>
                    TEL　078-855-6456
                </p>
                <p class="p-googlemap"><a class="p-common-btn" href="">WEBサイトを見る</a></p>
            </div>
        </div>
    </div>
    </section>
    
    <section>
    
    
    <div class="l-block10 disnone">  
        <div class="l-inner1160">  	
            <div class="l-block10-01">
                <h4><span class="Cinzel">Party Report</span>パーティレポート</h4>
                <div class="p-label"><label>パーティレポートを見る</label></div>
                <div class="l-block10-table">
                    <div>
                        <p class="p-image"><img src="/flower/common/img/img_block10_1_pc.png" alt=""></p>
                        <p class="p-message">
                            ダミーテキストです。ダミーテキストです。ダミーテキストです。ダミーテキストです。ダミーテキストです。ダミーテキストです。ダミーテキストです。ダミーテキストです。ダミーテキストです。ダミーテキストです。ダミーテキストです。ダミーテキストです。
                        </p>
                        <p class="p-btn"><a class="Cinzel" href="/party/">もっと見る</a></p>
                    </div><div>
                        <p class="p-image"><img src="/flower/common/img/img_block10_2_pc.png" alt=""></p>
                        <p class="p-message">
                            ダミーテキストです。ダミーテキストです。ダミーテキストです。ダミーテキストです。ダミーテキストです。ダミーテキストです。ダミーテキストです。ダミーテキストです。ダミーテキストです。ダミーテキストです。ダミーテキストです。ダミーテキストです。
                        </p>
                        <p class="p-btn"><a class="Cinzel" href="/party/">もっと見る</a></p>
                    </div><div>
                        <p class="p-image"><img src="/flower/common/img/img_block10_3_pc.png" alt=""></p>
                        <p class="p-message">
                            ダミーテキストです。ダミーテキストです。ダミーテキストです。ダミーテキストです。ダミーテキストです。ダミーテキストです。ダミーテキストです。ダミーテキストです。ダミーテキストです。ダミーテキストです。ダミーテキストです。ダミーテキストです。
                        </p>
                        <p class="p-btn"><a class="Cinzel" href="/party/">もっと見る</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
      
</div>
<?php include("../common/inc/footer.php"); ?>
</body>
</html>
