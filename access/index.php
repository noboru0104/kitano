<!doctype html>
<html lang="ja">
<head>
<?php include("../common/inc/head.php"); ?>
<title>ACCESS アクセス｜kitano garden</title>
<!-- ▼個別CSS▼ -->
<link rel="stylesheet" type="text/css" href="/access/common/styles/access.css">
<link rel="stylesheet" type="text/css" href="/common/styles/lightbox/lightbox.css">
<!-- ▲個別CSS▲ -->
<!-- ▼個別JS▼ -->
<script type="text/javascript" src="/access/common/js/access.js"></script>
<script type="text/javascript" src="/common/js/jquery.matchHeight/jquery.matchHeight.js"></script>
<script type="text/javascript" src="/common/js/lightbox/lightbox.js"></script>
<!-- ▲個別JS▲ -->
</head>
<body>
<?php include("../common/inc/header.php"); ?>
<div class="l-mvBlock">
    <div class="l-mv">
    	
    </div>
</div>    
<div id="wrapper">
    <section>
    <div class="l-block01">
    	<div class="l-inner">
            <h2><img class="is-imgChange" src="/access/common/img/h2_block01_pc.png" alt="ACCESS アクセス"><span><span class="Cinzel">Access</span>アクセス</span></h2>
            <div class="l-block01-map">
            	<img class="is-imgChange" src="/access/common/img/img_block01_pc.png" alt="kitano garden MAP">
            </div>
            <p class="p-googlemap"><a class="p-common-btn" href="https://goo.gl/maps/SKYfDqAojR32" target="_blank">GOOGLE MAP</a></p>
        	<p class="p-message">
            	〒650-0002 神戸市中央区北野町2-8-1<br>
                【TEL】078-241-8537<br>
                【営業時間】11:00～19:00<br>
                【定休日】水曜定休
            </p>
        </div>
    </div>
    </section>
    
    <section>
    <div class="l-block02">
    	<div class="l-inner">
            <div>
                <p class="p-title">電車にてお越しの場合</p>
                <p class="p-message">
                    JR・阪神・阪急各線「三宮」駅下車　徒歩12分<br>
                    JR新幹線・地下鉄各線「新神戸」駅下車　徒歩10分
                </p>
            </div>
            <div>
                <p class="p-title">お車にてお越しの場合</p>
                <p class="p-message">
                    阪神高速道路「京橋」「生田川」ランプより約5分<br>
                    提携駐車場をご用意しております。詳しくはパーキングマップをご覧ください。<br>
                    提携駐車場は数に限りがございます。できるだけ他の交通機関をご利用くださいますようお願い申し上げます。
                </p>
                <p class="p-btn"><a class="p-common-btn2" href="" target="_blank">PARKING MAP</a></p>
            </div>
            <div>
                <p class="p-title">神戸空港からお越しの場合</p>
                <p class="p-message">
                    ポートライナー「三宮」駅下車 徒歩約12分
                </p>
            </div>
        </div>
    </div>
    </section>  
    
    <?php include("../common/inc/pickupfair.php"); ?>
      
</div>
<?php include("../common/inc/footer.php"); ?>
</body>
</html>
