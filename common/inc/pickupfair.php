<div class="l-pickup">
    <div class="l-pickup-inner">
        <h2 class="l-pickup-ttl"><span class="l-pickup-ttl-en">Pick Up Fair</span><span class="l-pickup-ttl-jp">ピックアップフェア</span></h2>
        <div class="l-pickup-list">
            <div class="l-pickup-list-inner">
                <div class="l-pickup-item">
                    <div class="l-pickup-item-img"><img class="is-imgChange" src="/common/img/img_pickup-fair_01_pc.png" alt=""></div>
                    <div class="l-pickup-item-txtarea">
                        <p class="l-pickup-item-ttl">＜神戸フレンチ フルコース試食付＞<br>平日限定相談会</p>
                        <p class="l-pickup-item-txt">毎週火・木曜日　11:00～13:00<br>事前予約要</p>
                        <a href="/fair/" class="l-pickup-item-btn">もっと見る</a>
                    </div>
                </div>
                <div class="l-pickup-item">
                    <div class="l-pickup-item-img"><img class="is-imgChange" src="/common/img/img_pickup-fair_02_pc.png" alt=""></div>
                    <div class="l-pickup-item-txtarea">
                        <p class="l-pickup-item-ttl">＜神戸フレンチ フルコース試食付＞<br>平日限定相談会</p>
                        <p class="l-pickup-item-txt">毎週火・木曜日　11:00～13:00<br>事前予約要</p>
                        <a href="" class="l-pickup-item-btn">もっと見る</a>
                    </div>
                </div>
                <div class="l-pickup-item">
                    <div class="l-pickup-item-img"><img class="is-imgChange" src="/common/img/img_pickup-fair_03_pc.png" alt=""></div>
                    <div class="l-pickup-item-txtarea">
                        <p class="l-pickup-item-ttl">＜神戸フレンチ フルコース試食付＞<br>平日限定相談会</p>
                        <p class="l-pickup-item-txt">毎週火・木曜日　11:00～13:00<br>事前予約要</p>
                        <a href="" class="l-pickup-item-btn">もっと見る</a>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>