<div class="pickup_slide">
<div class="swiper-wrapper">

	<?php
	$today = date('Ymd');
	$fair_schedules = fair_schedule_list();
	$args = array(
		'post_type' => 'fair',
		'meta_query' => array( array(
			'key' => 'fair_pickup',
			'value' => 1
		)),
		'numberposts' => -1
	);
	$customPosts = get_posts($args);
	if($customPosts){ foreach($customPosts as $post){ setup_postdata( $post );
	$thumb = wp_get_attachment_image_src( get_field('fair_mainphoto_01') , 'fair_thumb' );

	//日付の配列を生成
	$fair_dates = array();
	$fair_dates = $fair_schedules[get_the_ID()]; //スケジュール取得;
	$fair_dates = array_keys($fair_dates);

	$fair_date = array();
	foreach( (array)$fair_dates as $date ){
		if(strtotime($date) >= strtotime($today)){//昨日までを削除
			$fair_date[] = $date;
		}
	}

	// sort($fair_dates);// ソート

	//時間の配列を生成
	// $fair_times = array();
	// foreach( (array)$fair_schedule as $key1 => $val1 ){
	// 	$fair_time = array_column($val1['fair_schedules_times'], 'fair_schedules_times_time');
	// 	foreach( (array)$fair_time as $time ){
	// 		$fair_times[] = $time;
	// 	}
	// };
	// sort($fair_times);// ソート
	// $unique_times = array_unique($fair_times);//重複を削除
	// $fair_times = array_values($unique_times);// キー番号を詰める

	?>
	<div class="item swiper-slide">
	<figure><a href="<?php the_permalink(); ?>"><img src="<?php echo $thumb[0]; ?>" alt=""></a></figure>
	<h5 class="white"><?php title_space_2_br() ?></h5>
	<p class="date">
		<?php
		if(get_field('bridalfair_archives_dates')){
			echo get_field('bridalfair_archives_dates');
		}else{
			foreach( (array)$fair_date as $date){
				$datestamp = strtotime($date);
				$w = date('w', $datestamp);
				echo date('n/d',$datestamp) . '（' . $week[$w] . '）';
				if ($date !== end($fair_dates)) {
					echo ', ';
				};
			};
		};
		?></p>
	<a href="<?php the_permalink(); ?>" class="more">詳しく見る</a>
   </div>
	<?php }};wp_reset_postdata(); ?>

</div>
	<ul class="nav">
		<li class="prev">　</li>
		<li class="next">　</li>
	</ul>
</div>
