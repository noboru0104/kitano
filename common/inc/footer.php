<footer>
    <div class="l-footer">
        <div class="l-footerBlock00">
            <div class="l-footerBlock01">
                <div class="l-footerBlock01-01">
                    <div class="l-title">
                        <p>INFORMATION<span class="Gothic">インフォメーション・アクセス</span></p>
                    </div>
                    <div class="l-adress">
                        <p class="Gothic">
                            〒650-0002 神戸市中央区北野町2-8-1<br>
                            【営業時間】11:00～19:00　水曜定休<br><br class="sponly1000">
                            【交通アクセス】
                        </p>
                        <div class="l-access Gothic">
                            <p>・電車で</p><p>JR・阪神・阪急各線「三宮」駅下車　徒歩12分<br>JR新幹線・地下鉄各線「新神戸」駅下車　徒歩10分</p>
                            <p>・お車で</p><p>阪神高速道路「京橋」「生田川」ランプより約5分<br>提携駐車場をご用意しております。詳しくはパーキングマップをご覧ください。提携駐車場は数に限りがございます。できるだけ他の交通機関をご利用くださいますようお願い申し上げます。</p>
                            <p>・神戸空港から</p><p>ポートライナー「三宮」駅下車　徒歩約12分</p>
                        </div>
                    </div>
                    <div class="l-btn">
                        <p><a href="/access/">もっと見る</a></p><p><a href="/guest/">ゲストの皆様へ</a></p>
                    </div>
                </div>
            </div><div class="l-footerBlock02">
                <div class="l-title">
                    <p>RESERVATION &amp; CONTACT<span class="Gothic">ご予約・お問い合わせ</span></p>
                </div>
                <div class="l-tel">
                    <p>お電話でのフェア予約</p>
                    <p>TEL<span>078-241-8537</span></p>
                    <p class="Gothic">11:00～19:00　水曜定休</p>
                </div>
                <div class="l-btn">
                    <p>
                        <a href="">フェア予約<span>BRIDAL FAIR</span></a>
                    </p><p>
                        <a href="">資料請求<span>REQUEST</span></a>
                    </p>
                </div>    
            </div>    
        </div>    
    </div>
    <div class="l-footerMenu">
    	<p class="p-logo"><img class="/" src="/common/img/img_footer_logo_pc.png" alt="kitano garden"></p>
        <div class="l-footerMenu01">
        	<ul class="clear flexbox">
            	<li><a href="/">ホーム</a></li><li><a href="/weddingstyle/">挙式</a></li><li><a href="/plan/">ウェディングプラン</a></li><li><a href="/cuisine/">お料理</a></li><li><a href="/flower/">装花</a></li><li><a href="/fair/">ブライダルフェア</a></li><li><a href="/access/">アクセス</a></li>
                <li><a href="/season/">春夏秋冬</a></li><li><a href="/party/">披露パーティ</a></li><li><a href="">ブライダルスタイル</a></li><li><a href="">ドレス</a></li><li><a href="/gallery/">フォトギャラリー</a></li><li><a href="/flow/">結婚式までの流れ</a></li><li><a href="/guest/">ゲストの皆様へ</a></li>
            </ul>
            
        </div>
    </div>
    <div class="l-copy">
    	<p>Copyright (c) KITANO gARDEN All rights reserved.</p>
    </div>
</footer>
