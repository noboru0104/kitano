<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="description" content="">
<!-- ▼共通CSS▼ -->
<link href="https://fonts.googleapis.com/css?family=Cinzel" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="/common/styles/reset.css">
<link rel="stylesheet" type="text/css" href="/common/styles/common.css">
<link rel="stylesheet" type="text/css" href="/common/styles/animate/animate.css">
<!-- ▲共通CSS▲ -->
<!-- ▼favicon▼ -->
<!--<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
<link rel="apple-touch-icon" sizes="180x180" href="images/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">-->
<!-- ▲favicon▲ -->
<!-- ▼共通JS▼ -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script type="text/javascript" src="/common/js/common.js"></script>
<!-- ▲共通JS▲ -->