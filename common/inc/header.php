<header>    
    <div class="l-header">
        <div class="l-header-table">
            <div class="l-header-left0">
                <!--SPハンバーガーメニュー-->
                <div id="toggle_block">                	
                    <div id="toggle">
                        <div id="toggle-btn">
                            <span id="toggle-btn-icon"></span><p>MENU</p>
                        </div>
                    </div><!--/#toggle-->
                </div><!--/#toggle_block-->
            </div>
            <div class="l-header-left">
                <h1><a href="/"><img class="/" src="/common/img/btn_header_logo_pc.png" alt="kitano garden"></a></h1>
            </div>
            <div class="l-header-center">
                <div class="l-menu">
                    <div><a href="/season/" data-page="season" data-title="l-season_menu">SEASON</a></div>
                    <div><a href="/ceremony/" data-page="ceremony" data-title="l-ceremony_menu">CEREMONY</a></div>
                    <div><a href="/party/" data-page="party" data-title="l-party_menu">PARTY</a></div>
                    <div><a href="/plan/" data-page="plan" data-title="l-plan_menu">PLAN</a></div>
                    <div><a href="/fair/" data-page="fair" data-title="l-fair_menu">FAIR</a></div>
                    <div><a href="/weddingstyle/" data-page="weddingstyle" data-title="l-wedding_menu">WEDDING</a></div>
                    <div><a href="/gallery/" data-page="gallery" data-title="l-gallery_menu">GALLERY</a></div>
                    <div><a href="/infomation/" data-page="infomation" data-title="l-infomation_menu">INFORMATION</a></div>
                    <div><a href="http://www.kitano-garden.com/restaurant/" target="_blank">RESTAURANT</a></div>
                </div>
                <div class="l-hover_menu">
                    <div class="l-season_menu" data-title="l-season_menu">
                        <a href="/season/#01">SPRING</a>-<a href="/season/#02">SUMMER</a>-<a href="/season/#03">AUTUMN</a>-<a href="/season/#04">WINTER</a>
                    </div>
                    <div class="l-ceremony_menu" data-title="l-ceremony_menu">
                        <a href="/ceremony/#01">CHAPEL</a>-<a href="/ceremony/#02">GARDEN</a>
                    </div>
                    <div class="l-party_menu" data-title="l-party_menu">
                        <a href="/party/#01">PIPA RED</a>-<a href="/party/#02">PIPA BLUE</a>-<a href="/party/#03">GARDEN PARTY</a>
                    </div>
                    <div class="l-plan_menu" data-title="l-plan_menu">
                        <a href="/weddingstyle/">WEDDING STYLE</a>-<a href="/plan/">WEDDING PLAN</a>
                    </div>
                    <div class="l-fair_menu" data-title="l-fair_menu">
                        <a href="">fair_test1</a>-<a href="">fair_test2</a>-<a href="">fair_test3</a>-<a href="">fair_test4</a>
                    </div>
                    <div class="l-wedding_menu" data-title="l-wedding_menu">
                        <a href="/cuisine/">CUISINE</a>-<a href="/">DRESS</a>-<a href="/flower/">FLOWER</a>
                    </div>
                    <!--<div class="l-gallery_menu" data-title="l-gallery_menu">
                        <a href="">gallery_test1</a>-<a href="">gallery_test2</a>-<a href="">gallery_test3</a>-<a href="">gallery_test4</a>
                    </div>-->
                    <div class="l-infomation_menu" data-title="l-infomation_menu">
                        <a href="/flow/">FLOW</a>-<a href="/access/">ACCESS</a>-<a href="/guest/">FOR GUEST</a>
                    </div>
                </div>
            </div>
            <div class="l-header-right">
            	<div class="p-contact">
                	<p class="p-tel">TEL<span>078-241-8537</span></p>
                	<p><a href=""><img class="" src="/common/img/btn_header_reserve_pc.png" alt="RESERVATION ご予約・資料請求・お問い合わせ"></a></p>
                </div>
                <!--SPハンバーガーメニュー-->
                <div id="toggle_block2" class="sponly1200">                	
                    <div id="toggle2">
                        <div id="toggle-btn2">
                            <img src="/common/img/btn_reservation_pc.png" alt="RESERVATION">
                            <span id="toggle-btn-icon2"></span><p>CLOSE</p>
                        </div>
                    </div><!--/#toggle-->
                </div><!--/#toggle_block-->
            </div>
        </div>
              
        <nav>
            <div id="global-nav-area" style="display:none;">
                <ul id="global-nav">
                    <li><p><a href="/season/"><span class="p-main">SEASON</span><span class="p-sub">春夏秋冬</span></a></p></li>
                    <li><p><a href="/ceremony/"><span class="p-main">CEREMONY</span><span class="p-sub">挙式</span></a></p></li>
                    <li><p><a href="/party/"><span class="p-main">PARTY</span><span class="p-sub">披露パーティ</span></a></p></li>
                    <li><p><a href="/plan/"><span class="p-main">PLAN</span><span class="p-sub">ウェディングプラン</span></a></p></li>
                    <li><p><a href="/fair/"><span class="p-main">FAIR</span><span class="p-sub">ブライダルフェア</span></a></p></li>
                    <li class="js-underlayer">
                    	<p>
                        	<span class="p-main">WEDDING</span><span class="p-sub js-menu">＋</span>
                        </p>
                    </li>                    
                    <ul class="js-global-nav-detail">
                        <li><a href="/cuisine/">CUISINE</a></li>
                        <li><a href="/">DRESS</a></li>
                        <li><a href="/flower/">FLOWER</a></li>
                    </ul>
                    <li><p><a href="/gallery/"><span class="p-main">GALLERY</span><span class="p-sub">ウェディングギャラリー</span></a></p></li>
                    <li class="js-underlayer">
                    	<p>
                        	<span class="p-main">INFORMATION</span><span class="p-sub js-menu">＋</span>
                        </p>
                    </li>                    
                    <ul class="js-global-nav-detail">
                        <li><a href="/flow/">FLOW</a></li>
                        <li><a href="/access/">ACCESS</a></li>
                        <li><a href="/guest/">FOR GUEST</a></li>
                    </ul>
                    <li class="p-last"><p><a href="http://www.kitano-garden.com/restaurant/" target="_blank"><span class="p-main">RESTAURANT</span><span class="p-sub">レストラン</span></a></p></li>
                </ul>
            </div>
            <div id="global-nav-area2" style="display:none;">
                <ul id="global-nav2">
                    <li class="p-title"><p>RESERVATION &amp; CONTACT<span>ご予約・お問い合わせ</span></p></li>
                    <li class="l-tel">
                    	<p>お電話でのフェア予約</p>
                        <p>TEL<span>078-241-8537</span></p>
                        <p>11:00～19:00　水曜定休</p>
                    </li>
                    <li class="l-btn">
                    	<p><a href="">フェア予約<span>BRIDAL FAIR</span></a></p>
                        <p><a href="">資料請求<span>REQUEST</span></a></p>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</header>