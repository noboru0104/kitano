$(function(){
	"use strict";
	ImgChange();
	FooterMenuChange();
	
	var currenturl = location.pathname;
	$('.l-header-center .l-menu > div').each( function(){
		var currentpage = '/' + $(this).find('a').data('page') + '/';
		if(currenturl === currentpage){
			$(this).find('a').addClass('p-current');	
		} else {
			$(this).find('a').removeClass('p-current');
		}
	});
	
	// TOGGLE NAV	
	$( "#global-nav-area" ).hide();
	//var flg = 'default';
	
	$( "#toggle" ).click( function(){
		SPMenuOPEN();
	});
	$( "#global-nav-area #global-nav li a" ).click( function(){
		SPMenuOPEN();
	});
	
	// TOGGLE NAV2	
	$( "#global-nav-area2" ).hide();
	//var flg = 'default';
	
	$( "#toggle2" ).click( function(){
		SPMenuOPEN2();
	});
	$( "#global-nav-area2 #global-nav2 li a" ).click( function(){
		SPMenuOPEN2();
	});
	
	var topBtn = $('#page-top');    
	topBtn.hide();
	
	//スクロールが500に達したらボタン表示
	$(window).scroll(function () {
		if ($(this).scrollTop() > 100) {
			topBtn.fadeIn();
		} else {
			topBtn.fadeOut();
		}
		
		//TOP以外のページの時にスクロールが一番上にある時ヘッダーの背景の透明度を無しにする。
		if($('#top').length){
			
		} else {
						
		}
	});
	
	$( ".js-global-nav-detail" ).hide();
	$( ".js-underlayer" ).click(
		function(){
			$(this).next().toggleClass( "show" );
	
			if( $(this).next().hasClass( "show" ) ){
				
				$(this).find(".js-menu").text( "―" );
				$(this).next().addClass( "show" );
	
			}else{
				
				$(this).find(".js-menu").text( "＋" );
				$(this).next().removeClass( "show" );
			}
	
			$(this).next().slideToggle('slow');
		}
	);
	
	$('.l-header-center .l-menu div a').hover(
		
		function(){
			//alert('a');
			var current = $('.' + $(this).data('title'));
			//alert(current);
			current.addClass('on_hover');
		},
		function(){
			var current = $('.' + $(this).data('title'));
			current.removeClass('on_hover');
		}
	);
	
	$('.l-hover_menu > div').hover(
		
		function(){
			//alert('a');
			var current = $('.' + $(this).data('title'));
			//alert(current);
			current.addClass('on_hover');
		},
		function(){
			var current = $('.' + $(this).data('title'));
			current.removeClass('on_hover');
		}
	);
	
	//スクロールしてトップ
	topBtn.click(function () {
		$('body,html').animate({
			scrollTop: 0
		}, 700);
		return false;
	});
	
	$(window).on('resize', function(){
		setTimeout(function(){
		//サイズ調整
			SideScroll();
			FooterMenuChange();
		},10);
	});
	
	$('.l-hover_menu a').click(function(){	
		//ヘッダーがfixedによるアンカー調整
		var widimage = window.innerWidth;
		var value = 0;
		setTimeout(function(){			
			var speed2 = 10;
			var urlHash = location.hash;
			var target2 = $(urlHash);
			var value = 0;
			//別ページからの遷移時
			if (urlHash) {
				value = $('header').height();
				var position2 = target2.offset().top - value;
				$("html, body").animate({scrollTop:position2}, speed2, "swing");
				return false;
			}
		},10);
	});	
	
	$(window).on('load', function(){
		//ヘッダーがfixedによるアンカー調整
		var widimage = window.innerWidth;
		var value = 0;
		setTimeout(function(){			
			var speed2 = 10;
			var urlHash = location.hash;
			var target2 = $(urlHash);
			var value = 0;
			//別ページからの遷移時
			if (urlHash) {
				value = $('header').height();
				var position2 = target2.offset().top - value;
				$("html, body").animate({scrollTop:position2}, speed2, "swing");
				return false;
			}
		},10);
		
		setTimeout(function(){
		//サイズ調整
			SideScroll();
			FooterMenuChange();
		},10);
	});		
		
	$('a[href^=#]').click(function(){	
		var speed = 700;
		var href= $(this).attr("href");
		var target = $(href === "#" || href === "" ? 'html' : href);
		var position = 0;
		if($(this).hasClass('is-pagescroll')){
			position = target.offset().top - $('header').height();
			$("html, body").animate({scrollTop:position}, speed, "swing");
			return false;
		} else {				
			position = target.offset().top;			
			$("html, body").animate({scrollTop:position}, speed, "swing");
			return false;					
		}
	});
	
	$(window).on('load resize', function(){
		ImgChange();
		SPMenuChange();
		setTimeout(function(){
			SPMenuHeight();
			FooterMenuChange();
		},100);
					
	});
	
	if(navigator.userAgent.indexOf('Android') > 0){
        
    }
	
});


function  ImgChange(){
	"use strict";
	// スマホ画像切替
	var widimage = window.innerWidth;
	//var widimage = parseInt($(window).width()) + 16;
	
	if( widimage < 1001 ){
		$('.is-imgChange').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_pc', '_sp'));
		});
	} else {
		
		$('.is-imgChange').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_sp', '_pc'));
		});
	}
	
	if( widimage < 769 ){
		$('.is-imgChange2').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_pc', '_sp'));
		});
	} else {
	
		$('.is-imgChange2').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_sp', '_pc'));
		});
	}
	
	if( widimage < 481 ){
		$('.is-imgChange3').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_pc', '_sp'));
		});
	} else {
	
		$('.is-imgChange3').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_sp', '_pc'));
		});
	}

}

function  SPMenuHeight(){
	"use strict";
	
	var header_Height = $('header').height();
	var menu_Height = window.innerHeight - header_Height;
	//var menu_Height = window.innerHeight - 150;
	$('#global-nav-area').css('top',header_Height + 'px');
	$('#global-nav-area').css('height',menu_Height + 'px');
	$('#global-nav-area').css('overflow-y','scroll');
	$('#global-nav-area').css('-webkit-overflow-scrolling','touch');
	
	$('#global-nav-area2').css('top',header_Height + 'px');
	$('#global-nav-area2').css('height',menu_Height + 'px');
	$('#global-nav-area2').css('overflow-y','scroll');
	$('#global-nav-area2').css('-webkit-overflow-scrolling','touch');
}
function  SPMenuOPEN(){
	"use strict";
	var flg = 'default';
	$( "#toggle-btn-icon" ).toggleClass( "close" );
	$( "#toggle" ).toggleClass( "show" );
	if($( "#toggle-btn-icon" ).hasClass( "close" )){
		$("#toggle-btn p").text("CLOSE");
	} else {
		$("#toggle-btn p").text("MENU");
	}
	if( flg === "default" ){
		
		flg = "close";

		$( "#global-nav-area" ).addClass( "show" );
		
	}else{
		
		flg = "default";
		$( "#global-nav-area" ).removeClass( "show" );
	}

	$("#global-nav-area").slideToggle('slow');
}
function  SPMenuOPEN2(){
	"use strict";
	var flg = 'default';
	$( "#toggle-btn-icon2" ).toggleClass( "close" );
	$( "#toggle2" ).toggleClass( "show" );
	if($( "#toggle-btn-icon2" ).hasClass( "close" )){
		$("#toggle-btn2 img").css('display','none');
	} else {
		$("#toggle-btn2 img").css('display','block');
	}

	if( flg === "default" ){
		
		flg = "close";

		$( "#global-nav-area2" ).addClass( "show" );
		
	}else{
		
		flg = "default";
		$( "#global-nav-area2" ).removeClass( "show" );
	}

	$("#global-nav-area2").slideToggle('slow');
}

function  SPMenuChange(){
	"use strict";
	
	var widimage = window.innerWidth;
	if( widimage < 1201 ){
		if($( "#toggle" ).hasClass( "show" )){
			$("#global-nav-area").css('display','block');
		} else {
			$("#global-nav-area").css('display','none');
		}
		if($( "#toggle2" ).hasClass( "show" )){
			$("#global-nav-area2").css('display','block');
		} else {
			$("#global-nav-area2").css('display','none');
		}
	} else {
		$("#global-nav-area").css('display','none');
		$("#global-nav-area2").css('display','none');
	}
}

function  SideScroll(){
	"use strict";
	
	var widimage = window.innerWidth;	
	if( widimage < 1001 ){
		
	} else {
		// スクロールした時に以下の処理
		ScrollPosition();
		$(window).bind("scroll", function() {
			// スクロールした時に以下の処理
			ScrollPosition();			
		});		
	}
}

function  ScrollPosition(){
	"use strict";
	
	var wst =  $(window).scrollTop();
	/*MV*/
	var move_height1 = 0;
	if($('.l-mvBlock').length){
		move_height1 = $(".l-mvBlock").outerHeight() - 30;
	} else if($('.l-mv').length){
		move_height1 = $(".l-mv").outerHeight();
	} else if($('.l-mv-under').length){
		move_height1 = $(".l-mv-under").outerHeight() - 30;
	} else if($('.l-block01').length){
		move_height1 = $(".l-block01").outerHeight() - 30;
	} else {
		move_height1 = 300;
	}
	
	/*ヘッダーを固定にするかの判断*/
	if( move_height1 < wst ){
		$("header").addClass('is-fix');
	} else {
		$("header").removeClass('is-fix');
	}	
}

function  FooterMenuChange(){
	"use strict";
	// スマホ画像切替
	var widimage = window.innerWidth;
	//var widimage = parseInt($(window).width()) + 16;
	
	if( widimage < 1201 ){
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(2) a').text('春夏秋冬');
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(2) a').attr('href','/season/');
		
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(3) a').text('挙式');
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(3) a').attr('href','/weddingstyle/');
		
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(4) a').text('披露パーティ');
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(4) a').attr('href','/party/');
		
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(5) a').text('ウェディングプラン');
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(5) a').attr('href','/plan/');
		
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(6) a').text('ウェディングスタイル');
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(6) a').attr('href','/weddingstyle/');
		
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(7) a').text('お料理');
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(7) a').attr('href','/cuisine/');
		
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(8) a').text('ドレス');
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(8) a').attr('href','');
		
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(9) a').text('装花');
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(9) a').attr('href','/flower/');
		
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(10) a').text('フォトギャラリー');
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(10) a').attr('href','/gallery/');
		
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(11) a').text('ブライダルフェア');
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(11) a').attr('href','/fair/');
		
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(12) a').text('結婚式までの流れ');
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(12) a').attr('href','/flow/');
		
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(13) a').text('アクセス');
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(13) a').attr('href','/access/');
	} else {
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(2) a').text('挙式');
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(2) a').attr('href','/weddingstyle/');
		
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(3) a').text('ウェディングプラン');
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(3) a').attr('href','/plan/');
		
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(4) a').text('お料理');
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(4) a').attr('href','/cuisine/');
		
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(5) a').text('装花');
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(5) a').attr('href','/flower/');
		
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(6) a').text('ブライダルフェア');
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(6) a').attr('href','/fair/');
		
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(7) a').text('アクセス');
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(7) a').attr('href','/access/');
		
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(8) a').text('春夏秋冬');
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(8) a').attr('href','/season/');
		
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(9) a').text('披露パーティ');
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(9) a').attr('href','/party/');
		
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(10) a').text('ウェディングスタイル');
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(10) a').attr('href','/weddingstyle/');
		
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(11) a').text('ドレス');
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(11) a').attr('href','');
		
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(12) a').text('フォトギャラリー');
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(12) a').attr('href','/gallery/');
		
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(13) a').text('結婚式までの流れ');
		$('.l-footerMenu .l-footerMenu01 ul li:nth-child(13) a').attr('href','/flow/');
	}

}



