
	
jQuery(document).ready(function($){
	"use strict";
	var slider = $('#slider9').bxSlider({
		auto:false,
		speed:1000,
		pause:4000,
		onSlideAfter:function($slideElement, oldIndex, newIndex){
			//var count = Number($('.cont').text());
//			$('.cont').text(count + 1);
		}
	});
	
	setTimeout(function(){
		//サイズ調整
		var widimage = window.innerWidth;
		if( widimage < 1001 ){
			slider.reloadSlider();
		}else{
			slider.destroySlider();
		}
	},10);
	
	// 初期表示時の実行
	$(window).on('load', function(){
		setTimeout(function(){
		//サイズ調整
			var widimage = window.innerWidth;
			if( widimage < 1001 ){
				slider.reloadSlider();
			}else{
				slider.destroySlider();
			}
		},10);
	});
	
	// リサイズ時の実行
	$(window).on('resize', function(){
		setTimeout(function(){
		//サイズ調整
			var widimage = window.innerWidth;
			if( widimage < 1001 ){
				slider.reloadSlider();
			}else{
				slider.destroySlider();
			}
		},10);
	});
});