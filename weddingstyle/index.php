<!doctype html>
<html lang="ja">
<head>
<?php include("../common/inc/head.php"); ?>
<title>Wedding Style-ウェディングスタイル | kitano garden</title>
<!-- ▼個別CSS▼ -->
<link rel="stylesheet" type="text/css" href="/weddingstyle/common/styles/weddingstyle.css">
<!-- ▲個別CSS▲ -->
<!-- ▼個別JS▼ -->
<!-- <script type="text/javascript" src="/common/js/jquery.bxslider/jquery.bxslider.min.js"></script> -->
<!-- <script type="text/javascript" src="/ceremony/common/js/ceremony.js"></script> -->
<!-- <script type="text/javascript" src="js/common/jquery.matchHeight.js"></script> -->
<!-- ▲個別JS▲ -->
</head>
<body>
<?php include("../common/inc/header.php"); ?>
<div id="wrapper">
    
    <div class="l-mv">
        <h2 class="l-mv-ttl-en">Wedding Style</h2>
        <p class="l-mv-ttl-jp">ウェディングスタイル</p>
    </div>
    <!-- / .l-mv -->
    
    <div class="l-mv-under">
        <h3 class="l-mv-under-ttl"><span class="l-mv-under-ttl-en">Choose Your Style</span><span class="l-mv-under-ttl-jp">選べるウェディングスタイル</span></h3>
        <div class="l-mv-under-nav">
            <div class="l-mv-under-nav-item">
                <div class="l-mv-under-nav-img"><img class="is-imgChange" src="/weddingstyle/common/img/img_block01_01_pc.jpg" alt=""></div>
                <div class="l-mv-under-nav-inner">
                    <p class="l-mv-under-nav-txt"><a href="#block01" class="l-mv-under-nav-link01"><span>組み合わせ<br class="sponly1000">から選ぶ</span></a></p>
                    <ul class="l-mv-under-nav-list">
                        <li><a href="">挙式＋披露宴</a></li>
                        <li><a href="">披露宴のみ</a></li>
                        <li><a href="">挙式＋会食</a></li>
                    </ul>
                </div>
            </div>
            <div class="l-mv-under-nav-item">
                <div class="l-mv-under-nav-img"><img class="is-imgChange" src="/weddingstyle/common/img/img_block01_02_pc.jpg" alt=""></div>
                <div class="l-mv-under-nav-inner">
                    <p class="l-mv-under-nav-txt"><a href="#block02" class="l-mv-under-nav-link01"><span>時期で選ぶ</span></a></p>
                    <ul class="l-mv-under-nav-list">
                        <li><a href="">10ヶ月以内の結婚式</a></li>
                        <li><a href="">1年先ぐらい～</a></li>
                        <li><a href="">お急ぎ</a></li>
                    </ul>
                </div>
            </div>
            <div class="l-mv-under-nav-item">
                <div class="l-mv-under-nav-img"><img class="is-imgChange" src="/weddingstyle/common/img/img_block01_03_pc.jpg" alt=""></div>
                <div class="l-mv-under-nav-inner">
                    <p class="l-mv-under-nav-txt"><a href="#block03" class="l-mv-under-nav-link01"><span>目的に<br class="sponly1000">合わせて選ぶ</span></a></p>
                    <div class="l-mv-under-nav-list column2">
                        <ul>
                            <li><a href="">お料理重視</a></li>
                            <li><a href="">予算重視</a></li>
                            <li><a href="">少人数</a></li>
                        </ul>
                        <ul>
                            <li><a href="">親族で食事のみ</a></li>
                            <li><a href="">おひろめ</a></li>
                            <li><a href="">マタニティ</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- / .l-mv-under-nav -->
    </div>
    <!-- / .l-mv-under -->
    
    <div id="block01" class="l-block01 l-block">
        <h3 class="l-block-main-ttl"><span class="l-block-main-ttl-en">Combination</span><span class="l-block-main-ttl-jp">組み合わせから選ぶ</span></h3>
        <div class="l-block-inner">
            <div class="l-block-list">
                <div class="l-block-list-item">
                    <div class="l-block-list-img"><img class="is-imgChange" src="/weddingstyle/common/img/img_block02_01_pc.jpg" alt=""></div>
                    <div class="l-block-list-txtarea">
                        <div class="l-block-list-txtarea-inner">
                            <h4 class="l-block-list-ttl">挙式＋披露宴</h4>
                            <p class="l-block-list-txt">スタンダードな挙式と披露宴の<br class="sponly1000">ご結婚式をお考えの方</p>
                            <ul class="l-block-list-option">
                                <li>40名様～90名様まで可能</li>
                                <li>1ヶ月前まで申込OK</li>
                            </ul>
                            <p class="l-block-list-btn"><a href="" class="l-button01">プランを見る</a></p>
                        </div>
                    </div>
                </div>
                <div class="l-block-list-item">
                    <div class="l-block-list-img"><img class="is-imgChange" src="/weddingstyle/common/img/img_block02_02_pc.jpg" alt=""></div>
                    <div class="l-block-list-txtarea">
                        <div class="l-block-list-txtarea-inner">
                            <h4 class="l-block-list-ttl">披露宴のみ</h4>
                            <p class="l-block-list-txt">海外挙式や神社での挙式後に、<br class="sponly1000">披露宴のみをご予定の方</p>
                            <ul class="l-block-list-option">
                                <li>40名様～90名様まで可能</li>
                                <li>1ヶ月前まで申込OK</li>
                            </ul>
                            <p class="l-block-list-btn"><a href="" class="l-button01">プランを見る</a></p>
                        </div>
                    </div>
                </div>
                <div class="l-block-list-item">
                    <div class="l-block-list-img"><img class="is-imgChange" src="/weddingstyle/common/img/img_block02_03_pc.jpg" alt=""></div>
                    <div class="l-block-list-txtarea">
                        <div class="l-block-list-txtarea-inner">
                            <h4 class="l-block-list-ttl">挙式＋会食</h4>
                            <p class="l-block-list-txt">挙式と身近なご親族様との<br class="sponly1000">会食でお考えの方</p>
                            <ul class="l-block-list-option">
                                <li>20名様から可能</li>
                                <li>1ヶ月前まで申込OK</li>
                            </ul>
                            <p class="l-block-list-btn"><a href="" class="l-button01">プランを見る</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- / .l-block-list -->
            <p class="l-block-more"><a href="/plan/" class="l-button02">
                <span class="l-button02-jp">プラン一覧を見る</span>
                <span class="l-button02-en">WEDDING PLAN</span>
            </a></p>
        </div>
    </div>
    <!-- / .block01 -->
    
    <div id="block02" class="l-block02 l-block">
        <h3 class="l-block-main-ttl"><span class="l-block-main-ttl-en">Season</span><span class="l-block-main-ttl-jp">時期で選ぶ</span></h3>
        <div class="l-block-inner">
            <div class="l-block-list">
                <div class="l-block-list-item">
                    <div class="l-block-list-img"><img class="is-imgChange" src="/weddingstyle/common/img/img_block03_01_pc.jpg" alt=""></div>
                    <div class="l-block-list-txtarea">
                        <div class="l-block-list-txtarea-inner">
                            <h4 class="l-block-list-ttl">10ヶ月以内の結婚式</h4>
                            <p class="l-block-list-txt">約半年後くらい、または<br>1年以内くらいでの結婚式をご予定の方</p>
                            <ul class="l-block-list-option">
                                <li>40名様～90名様まで可能</li>
                                <li>1ヶ月前まで申込OK</li>
                            </ul>
                            <p class="l-block-list-btn"><a href="" class="l-button01">プランを見る</a></p>
                        </div>
                    </div>
                </div>
                <div class="l-block-list-item">
                    <div class="l-block-list-img"><img class="is-imgChange" src="/weddingstyle/common/img/img_block03_02_pc.jpg" alt=""></div>
                    <div class="l-block-list-txtarea">
                        <div class="l-block-list-txtarea-inner">
                            <h4 class="l-block-list-ttl">1年先ぐらい～</h4>
                            <p class="l-block-list-txt">ゆっくり準備して1年後くらいの結婚式、<br>またはそれより先のご予定の方</p>
                            <ul class="l-block-list-option">
                                <li>40名様～90名様まで可能</li>
                                <li>最長18ヶ月先までのご予約可能</li>
                            </ul>
                            <p class="l-block-list-btn"><a href="" class="l-button01">プランを見る</a></p>
                        </div>
                    </div>
                </div>
                <div class="l-block-list-item">
                    <div class="l-block-list-img"><img class="is-imgChange" src="/weddingstyle/common/img/img_block03_03_pc.jpg" alt=""></div>
                    <div class="l-block-list-txtarea">
                        <div class="l-block-list-txtarea-inner">
                            <h4 class="l-block-list-ttl">お急ぎ</h4>
                            <p class="l-block-list-txt">マタニティや少人数で<br class="sponly1000">直近で結婚式をご予定の方</p>
                            <ul class="l-block-list-option">
                                <li>20名様から可能（マタニティは30名様から）</li>
                                <li>1ヶ月前まで申込OK</li>
                            </ul>
                            <p class="l-block-list-btn"><a href="" class="l-button01">プランを見る</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- / .l-block-list -->
            <p class="l-block-more"><a href="/season/" class="l-button02">
                <span class="l-button02-jp">プラン一覧を見る</span>
                <span class="l-button02-en">WEDDING PLAN</span>
            </a></p>
        </div>
    </div>
    <!-- / .block02 -->
    
    <div id="block03" class="l-block03 l-block">
        <h3 class="l-block-main-ttl"><span class="l-block-main-ttl-en">Purpose</span><span class="l-block-main-ttl-jp">目的に合わせて選ぶ</span></h3>
        <div class="l-block-inner">
            <div class="l-block-list">
                <div class="l-block-list-item">
                    <div class="l-block-list-img"><img class="is-imgChange" src="/weddingstyle/common/img/img_block04_01_pc.jpg" alt=""></div>
                    <div class="l-block-list-txtarea">
                        <div class="l-block-list-txtarea-inner">
                            <h4 class="l-block-list-ttl">お料理重視</h4>
                            <p class="l-block-list-txt">おもてなしをしっかりと、<br>お料理にこだわったご結婚式をご希望の方</p>
                            <ul class="l-block-list-option">
                                <li>40名様～90名様まで可能</li>
                                <li>1ヶ月前まで申込OK</li>
                            </ul>
                            <p class="l-block-list-btn"><a href="" class="l-button01">プランを見る</a></p>
                        </div>
                    </div>
                </div>
                <div class="l-block-list-item">
                    <div class="l-block-list-img"><img class="is-imgChange" src="/weddingstyle/common/img/img_block04_02_pc.jpg" alt=""></div>
                    <div class="l-block-list-txtarea">
                        <div class="l-block-list-txtarea-inner">
                            <h4 class="l-block-list-ttl">予算重視</h4>
                            <p class="l-block-list-txt">予算も重視してお得に<br class="sponly1000">結婚式をしたいとご希望の方</p>
                            <ul class="l-block-list-option">
                                <li>40名様～90名様まで可能</li>
                                <li>1ヶ月前まで申込OK</li>
                            </ul>
                            <p class="l-block-list-btn"><a href="" class="l-button01">プランを見る</a></p>
                        </div>
                    </div>
                </div>
                <div class="l-block-list-item">
                    <div class="l-block-list-img"><img class="is-imgChange" src="/weddingstyle/common/img/img_block04_03_pc.jpg" alt=""></div>
                    <div class="l-block-list-txtarea">
                        <div class="l-block-list-txtarea-inner">
                            <h4 class="l-block-list-ttl">少人数</h4>
                            <p class="l-block-list-txt">20～30名くらいで、<br>身近なゲストとアットホームな<br class="sponly1000">結婚式をご希望の方</p>
                            <ul class="l-block-list-option">
                                <li>20名様から可能</li>
                                <li>1ヶ月前まで申込OK</li>
                            </ul>
                            <p class="l-block-list-btn"><a href="" class="l-button01">プランを見る</a></p>
                        </div>
                    </div>
                </div>
                <div class="l-block-list-item">
                    <div class="l-block-list-img"><img class="is-imgChange" src="/weddingstyle/common/img/img_block04_04_pc.jpg" alt=""></div>
                    <div class="l-block-list-txtarea">
                        <div class="l-block-list-txtarea-inner">
                            <h4 class="l-block-list-ttl">親族で食事のみ</h4>
                            <p class="l-block-list-txt">ご親族中心でのお披露目を<br class="sponly1000">兼ねたお食事会だけをご検討の方</p>
                            <ul class="l-block-list-option">
                                <li>20名様から可能</li>
                                <li>1ヶ月前まで申込OK</li>
                            </ul>
                            <p class="l-block-list-btn"><a href="" class="l-button01">プランを見る</a></p>
                        </div>
                    </div>
                </div>
                <div class="l-block-list-item">
                    <div class="l-block-list-img"><img class="is-imgChange" src="/weddingstyle/common/img/img_block04_05_pc.jpg" alt=""></div>
                    <div class="l-block-list-txtarea">
                        <div class="l-block-list-txtarea-inner">
                            <h4 class="l-block-list-ttl">おひろめ</h4>
                            <p class="l-block-list-txt">挙式をせずに、<br class="sponly1000">お披露目パーティーだけをお考えの方</p>
                            <ul class="l-block-list-option">
                                <li>40名様～90名様まで可能</li>
                                <li>1ヶ月前まで申込OK</li>
                            </ul>
                            <p class="l-block-list-btn"><a href="" class="l-button01">プランを見る</a></p>
                        </div>
                    </div>
                </div>
                <div class="l-block-list-item">
                    <div class="l-block-list-img"><img class="is-imgChange" src="/weddingstyle/common/img/img_block04_06_pc.jpg" alt=""></div>
                    <div class="l-block-list-txtarea">
                        <div class="l-block-list-txtarea-inner">
                            <h4 class="l-block-list-ttl">マタニティ</h4>
                            <p class="l-block-list-txt">マタニティウェディングのご予定を<br class="sponly1000">されておられる方</p>
                            <ul class="l-block-list-option">
                                <li>30名様から可能</li>
                                <li>1ヶ月前まで申込OK</li>
                            </ul>
                            <p class="l-block-list-btn"><a href="" class="l-button01">プランを見る</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- / .l-block-list -->
            <p class="l-block-more"><a href="" class="l-button02">
                <span class="l-button02-jp">プラン一覧を見る</span>
                <span class="l-button02-en">WEDDING PLAN</span>
            </a></p>
        </div>
    </div>
    <!-- / .block03 -->
    
    
    
</div>
<!-- / #wrapper -->
<?php include("../common/inc/footer.php"); ?>
</body>
</html>
