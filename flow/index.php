<!doctype html>
<html lang="ja">
<head>
<?php include("../common/inc/head.php"); ?>
<title>Flow-結婚式までの流れ | kitano garden</title>
<!-- ▼個別CSS▼ -->
<link rel="stylesheet" type="text/css" href="/flow/common/styles/flow.css">
<!-- ▲個別CSS▲ -->
<!-- ▼個別JS▼ -->
<!-- <script type="text/javascript" src="js/top.js"></script> -->
<!-- <script type="text/javascript" src="js/common/jquery.matchHeight.js"></script> -->
<!-- ▲個別JS▲ -->
</head>
<body>
<?php include("../common/inc/header.php"); ?>
<div id="wrapper">
    
    <div class="l-mv"></div>
    
    <div class="l-content">
        <div class="l-content-inner">
            <div class="l-content-inner-border01">
                <div class="l-mv-ttl">
                    <div class="l-mv-ttl-inner">
                        <h2 class="l-mv-ttl-en">Flow</h2>
                        <p class="l-mv-ttl-jp">結婚式までの流れ</p>
                    </div>
                </div>
                <!-- / .l-catch-ttl01 -->
                
                <div class="l-flowindex">
                    <h3 class="l-flowindex-ttl">最高の一日のために</h3>
                    <p class="l-flowindex-txt">はじめての結婚式。期待も高まる分、不安なことも多いですよね。<br>会場見学からご結婚式まで、安心して北野ガーデンでは最高のウェディングを叶えていただくための流れをご紹介いたします。</p>
                    <ol class="l-flowindex-list">
                        <li><a href="#block01">
                            <p class="l-flowindex-list-item">
                                <span class="l-flowindex-list-ttl">会場見学</span>
                                <span class="l-flowindex-list-txt">Wedding Fair</span>
                            </p>
                        </a></li>
                        <li><a href="#block02">
                            <p class="l-flowindex-list-item">
                                <span class="l-flowindex-list-ttl">お打合せ</span>
                                <span class="l-flowindex-list-txt">Meating</span>
                            </p>
                        </a></li>
                        <li><a href="#block03">
                            <p class="l-flowindex-list-item">
                                <span class="l-flowindex-list-ttl">衣装<br>お料理選定</span>
                                <span class="l-flowindex-list-txt">Dress &amp; Cuisine</span>
                            </p>
                        </a></li>
                        <li><a href="#block04">
                            <p class="l-flowindex-list-item">
                                <span class="l-flowindex-list-ttl">事前<br>リハーサル</span>
                                <span class="l-flowindex-list-txt">Rehearsal</span>
                            </p>
                        </a></li>
                    </ol>
                </div>
                <!-- / .l-flowindex -->
                
                <div id="block01" class="l-block01 l-block">
                    <h3 class="l-block-ttl01"><span class="l-block-ttl-num">1</span><span class="l-block-ttl-main">会場見学</span></h3>
                    <div class="l-block-wrap">
                        <div class="l-block-img"><img class="is-imgChange" src="/flow/common/img/img_block01_01_pc.jpg" alt=""></div>
                        <div class="l-block-txtarea">
                            <p class="l-block-ttl02-en">Wedding Fair</p>
                            <h4 class="l-block-ttl02">ブライダルフェア・見学＆相談会、試食会</h4>
                            <p class="l-block-txt">実際に挙式や披露宴会場、館内施設を当日のイメージを膨らませながら見学していただきます。またお二人のことについてお伺いし、ぴったりのプランニングをご提案。結婚式までのスケジュールやお見積りやご日程の空き状況もご案内させていただきます。ご見学と合わせての試食フェアも人気です。</p>
                            <dl class="l-block-point mt20">
                                <dt>POINT</dt>
                                <dd>
                                    <ul class="l-block-point-list">
                                        <li>会場の雰囲気を体感しイメージをふくらませる</li>
                                        <li>結婚式の費用やスケジュールなどをわかりやすくご案内</li>
                                    </ul>
                                </dd>
                            </dl>
                        </div>
                    </div>
                    <p class="l-block-link">
                        <a href="/fair/" class="l-button01">
                            <span class="l-button01-jp">フェア一覧を見る</span>
                            <span class="l-button01-en">BRIDAL FAIR</span>
                        </a>
                    </p>
                </div>
                <!-- / .l-block01 -->
                
                <div id="block02" class="l-block02 l-block">
                    <h3 class="l-block-ttl01"><span class="l-block-ttl-num">2</span><span class="l-block-ttl-main">お打合せ</span></h3>
                    <div class="l-block-wrap">
                        <div class="l-block-img"><img class="is-imgChange" src="/flow/common/img/img_block02_01_pc.jpg" alt=""></div>
                        <div class="l-block-txtarea">
                            <p class="l-block-ttl02-en">Planner Meating</p>
                            <h4 class="l-block-ttl02">専属プランナーとの打合せ</h4>
                            <p class="l-block-txt">おふたりに寄り添う気持ちで、専属のプランナーが担当させていただきます。<br>ご希望やイメージをしっかりとヒアリングさせていただき、オリジナルのウェディングをプランニング。お2人が安心して結婚式のご準備を進めていただけるように、サポートさせていただきます。</p>
                            <dl class="l-block-point">
                                <dt>POINT</dt>
                                <dd>
                                    <ul class="l-block-point-list">
                                        <li>オリジナリティあふれるプランニングのご提案</li>
                                        <li>丁寧な打合せと万全のサポートで安心</li>
                                    </ul>
                                </dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <!-- / .l-block02 -->
                
                <div id="block03" class="l-block03 l-block">
                    <h3 class="l-block-ttl01"><span class="l-block-ttl-num">3</span><span class="l-block-ttl-main">衣装・お料理選定</span></h3>
                    <div class="l-block-wrap l-block-wrap01">
                        <div class="l-block-img l-block-img01"><img class="is-imgChange" src="/flow/common/img/img_block03_01_pc.jpg" alt=""></div>
                        <div class="l-block-txtarea l-block-txtarea01">
                            <p class="l-block-ttl02-en">Dress Cordinate</p>
                            <h4 class="l-block-ttl02">衣装の試着・決定</h4>
                            <p class="l-block-txt">バリエーション豊かなドレスの中から私だけの運命の一着を！<br>複数のドレスショップで気に入るドレスが見つかるまで何度でもご試着を☆<br>プロのスタイリストがサポートいたします。</p>
                            <dl class="l-block-point">
                                <dt>POINT</dt>
                                <dd>
                                    <ul class="l-block-point-list">
                                        <li>ドレスの種類がとっても多い、小物や和装も充実</li>
                                        <li>体型やお悩みに合わせたフィッティング</li>
                                    </ul>
                                </dd>
                            </dl>
                            <p class="l-block-link01"><a href="/dress/" class="l-button02">ドレスを見る</a></p>
                        </div>
                    </div>
                    <div class="l-block-wrap l-block-wrap02">
                        <div class="l-block-img l-block-img02"><img class="is-imgChange" src="/flow/common/img/img_block03_02_pc.jpg" alt=""></div>
                        <div class="l-block-txtarea l-block-txtarea02">
                            <p class="l-block-ttl02-en">Cuisine &amp; Cake</p>
                            <h4 class="l-block-ttl02">料理・ケーキなどの打合せ</h4>
                            <p class="l-block-txt">北野ガーデンではお料理にこだわりたいというカップルがとっても多く、ゲストからも好評です。実際のメニューを試食してお2人の希望のオリジナルメニューも。</p>
                            <dl class="l-block-point">
                                <dt>POINT</dt>
                                <dd>
                                    <ul class="l-block-point-list">
                                        <li>季節に合わせたメニューがお楽しみ</li>
                                        <li>こだわりのウェディングケーキやオリジナルメニューも可能</li>
                                    </ul>
                                </dd>
                            </dl>
                            <p class="l-block-link01"><a href="/cuisine/" class="l-button02">料理を見る</a></p>
                        </div>
                    </div>
                </div>
                <!-- / .l-block03 -->
                
                <div id="block04" class="l-block04 l-block">
                    <h3 class="l-block-ttl01"><span class="l-block-ttl-num">4</span><span class="l-block-ttl-main">事前リハーサル</span></h3>
                    <div class="l-block-wrap">
                        <div class="l-block-img"><img class="is-imgChange" src="/flow/common/img/img_block04_01_pc.jpg" alt=""></div>
                        <div class="l-block-txtarea">
                            <p class="l-block-ttl02-en">Rehearsal</p>
                            <h4 class="l-block-ttl02">事前のリハーサル</h4>
                            <p class="l-block-txt">実際の流れをつかみながら、リハーサルを行い準備万端☆<br>あとは本番を待つばかりです！</p>
                        </div>
                    </div>
                </div>
                <!-- / .l-block04 -->
                
                
                <div class="l-block05 l-block l-block-head-arrow">
                    <p class="l-block-ttl-center-en">Wedding Day</p>
                    <h3 class="l-block-ttl-center">結婚式当日</h3>
                    <p class="l-block-txt-center">最高の一日を、ゲストとともに満喫して、<br>一生忘れられないウェディングに。</p>
                    <div class="l-block05-img"><img class="is-imgChange" src="/flow/common/img/img_block05_01_pc.jpg" alt=""></div>
                </div>
                <!-- / .l-block05 -->
                
                <div class="l-block06 l-block l-block-head-arrow">
                    <p class="l-block-ttl-center-en">After<br>The Wedding Day</p>
                    <h3 class="l-block-ttl-center">結婚式後</h3>
                    <p class="l-block-txt-center">いつでも帰ってこれる場所、それが北野ガーデン</p>
                    <div class="l-block-wrap">
                        <div class="l-block-img"><img class="is-imgChange" src="/flow/common/img/img_block06_01_pc.jpg" alt=""></div>
                        <div class="l-block-txtarea">
                            <h4 class="l-block-ttl03"><span>レストランとしても</span><span>ご利用いただけます</span></h4>
                            <p class="l-block-txt">かけがえのない大切を迎えた場所、そこにぜひまた帰ってきてください。<br>結婚記念日やお誕生日など、想いでの場所で、想いでのフレンチを夫婦になったあとも、いつでも帰っていらしてください。<br>スタッフ全員で心よりお待ちしております！</p>
                        </div>
                    </div>
                    <p class="l-block-link">
                        <a href="http://www.kitano-garden.com/restaurant/" class="l-button01" target="_blank">
                            <span class="l-button01-jp">レストランページを見る</span>
                            <span class="l-button01-en">RESTAURANT</span>
                        </a>
                    </p>
                </div>
                <!-- / .l-block06 -->
                
            </div>
            <!-- / .l-content-inner-border01 -->
        </div>
    </div>
    <!-- / .l-content -->
    
    <?php include("../common/inc/pickupfair.php"); ?>
    
</div>
<!-- / #wrapper -->
<?php include("../common/inc/footer.php"); ?>
</body>
</html>
