<!doctype html>
<html lang="ja">
<head>
<?php include("../common/inc/head.php"); ?>
<title>Party-披露宴会場 | kitano garden</title>
<!-- ▼個別CSS▼ -->
<link rel="stylesheet" type="text/css" href="/party/common/styles/party.css">
<!-- ▲個別CSS▲ -->
<!-- ▼個別JS▼ -->
<script type="text/javascript" src="/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="/party/common/js/party.js"></script>
<!-- <script type="text/javascript" src="js/common/jquery.matchHeight.js"></script> -->
<!-- ▲個別JS▲ -->
</head>
<body>
<?php include("../common/inc/header.php"); ?>
<div id="wrapper">
    
    <div class="l-mv-line">
        <div class="l-mv-line-inner">
            <h2 class="l-mv-line-en">Party</h2>
            <p class="l-mv-line-jp">披露宴会場</p>
        </div>
    </div>
    <!-- / .l-mv -->
    
    <div class="l-mv-under">
        <h3 class="l-mv-under-ttl">選べる2つのバンケットと<br class="sponly1000">ガーデンパーティー</h3>
        <ul class="l-mv-under-nav">
            <li><a href="#01" class="l-mv-under-nav-link01 is-pagescroll">
                <spna class="l-mv-under-nav-link-inner">
                    <span class="l-mv-under-nav-link01-small">会場１</span>
                    <span class="l-mv-under-nav-link01-main">Pipa Red</span>
                    <span class="l-mv-under-nav-link01-sub">ピパレッド</span>
                </spna>
            </a></li>
            <li><a href="#02" class="l-mv-under-nav-link02 is-pagescroll">
                <spna class="l-mv-under-nav-link01-inner">
                    <span class="l-mv-under-nav-link01-small">会場2</span>
                    <span class="l-mv-under-nav-link01-main">Pipa Blue</span>
                    <span class="l-mv-under-nav-link01-sub">ピパブルー</span>
                </spna>
            </a></li>
            <li><a href="#03" class="l-mv-under-nav-link03 is-pagescroll">
                <spna class="l-mv-under-nav-link01-inner">
                    <span class="l-mv-under-nav-link01-main">Garden Party</span>
                    <span class="l-mv-under-nav-link01-sub">ガーデンパーティ演出</span>
                </spna>
            </a></li>
        </ul>
    </div>
    <!-- / .l-mv-under -->
    
    <section id="01" class="l-block01 l-image-slide">
        <div class="l-image-slide-mv">
            <div class="l-image-slide-mv-img"><img class="is-imgChange" src="/party/common/img/img_block01_pc.jpg" alt=""></div>
            <h3 class="l-image-slide-mv-ttl"><span class="l-image-slide-mv-ttl-en">Pipa Red</span><span class="l-image-slide-mv-ttl-jp">ピパレッド</span></h3>
        </div>
        <div class="l-image-slide-under">
            <h4 class="l-image-slide-under-ttl">information</h4>
            <div class="l-image-slide-under-spec">
                <table>
                    <tbody>
                        <tr>
                            <th>収容人数</th>
                            <td>最大90名</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="l-image-slide-photo-wrap">
            <ul class="l-image-slide-photo" id="slider01">
                <li><img src="/party/common/img/img_block01_01.jpg" alt=""></li>
                <li><img src="/party/common/img/img_block01_02.jpg" alt=""></li>
                <li><img src="/party/common/img/img_block01_03.jpg" alt=""></li>
                <li><img src="/party/common/img/img_block01_04.jpg" alt=""></li>
                <li><img src="/party/common/img/img_block01_05.jpg" alt=""></li>
                <li><img src="/party/common/img/img_block01_06.jpg" alt=""></li>
                <li><img src="/party/common/img/img_block01_07.jpg" alt=""></li>
                <li><img src="/party/common/img/img_block01_08.jpg" alt=""></li>
                <li><img src="/party/common/img/img_block01_09.jpg" alt=""></li>
            </ul>
        </div>
    </section>
    <!-- / .l-block01 -->
    
    <section id="02" class="l-block02 l-image-slide">
        <div class="l-image-slide-mv">
            <div class="l-image-slide-mv-img"><img class="is-imgChange" src="/party/common/img/img_block02_pc.jpg" alt=""></div>
            <h3 class="l-image-slide-mv-ttl"><span class="l-image-slide-mv-ttl-en">Pipa Blue</span><span class="l-image-slide-mv-ttl-jp">ピパブルー</span></h3>
        </div>
        <div class="l-image-slide-under">
            <h4 class="l-image-slide-under-ttl">information</h4>
            <div class="l-image-slide-under-spec">
                <table>
                    <tbody>
                        <tr>
                            <th>収容人数</th>
                            <td>最大60名</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="l-image-slide-photo-wrap">
            <ul class="l-image-slide-photo" id="slider02">
                <li><img src="/party/common/img/img_block02_01.jpg" alt=""></li>
                <li><img src="/party/common/img/img_block02_02.jpg" alt=""></li>
                <li><img src="/party/common/img/img_block02_03.jpg" alt=""></li>
                <li><img src="/party/common/img/img_block02_04.jpg" alt=""></li>
                <li><img src="/party/common/img/img_block02_05.jpg" alt=""></li>
                <li><img src="/party/common/img/img_block02_06.jpg" alt=""></li>
                <li><img src="/party/common/img/img_block02_07.jpg" alt=""></li>
                <li><img src="/party/common/img/img_block02_08.jpg" alt=""></li>
                <li><img src="/party/common/img/img_block02_09.jpg" alt=""></li>
                <li><img src="/party/common/img/img_block02_10.jpg" alt=""></li>
            </ul>
        </div>
    </section>
    <!-- / .l-block02 -->
    
    <section id="03" class="l-block03 l-image-slide">
        <div class="l-image-slide-mv">
            <div class="l-image-slide-mv-img"><img class="is-imgChange" src="/party/common/img/img_block03_pc.jpg" alt=""></div>
            <h3 class="l-image-slide-mv-ttl"><span class="l-image-slide-mv-ttl-en">Garden Party</span><span class="l-image-slide-mv-ttl-jp">ガーデンパーティ</span></h3>
        </div>
        <div class="l-image-slide-under">
            <h4 class="l-image-slide-under-ttl">information</h4>
            <div class="l-image-slide-under-spec">
                <table>
                    <tbody>
                        <tr>
                            <th>シチュエーション</th>
                            <td>
                                <ul class="l-circle01">
                                    <li>バルーンリリース</li>
                                    <li>乾杯</li>
                                    <li>ケーキ入刀</li>
                                    <li>デザートビュッフェ</li>
                                    <li>デコレーション等</li>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="l-image-slide-photo-wrap">
            <ul class="l-image-slide-photo" id="slider03">
                <li><img src="/party/common/img/img_block03_01.jpg" alt=""></li>
                <li><img src="/party/common/img/img_block03_02.jpg" alt=""></li>
                <li><img src="/party/common/img/img_block03_03.jpg" alt=""></li>
                <li><img src="/party/common/img/img_block03_04.jpg" alt=""></li>
                <li><img src="/party/common/img/img_block03_05.jpg" alt=""></li>
            </ul>
        </div>
    </section>
    <!-- / .l-block03 -->
    
</div>
<!-- / #wrapper -->
<?php include("../common/inc/footer.php"); ?>
</body>
</html>
