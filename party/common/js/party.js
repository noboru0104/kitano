$(function() {
	var config = {
		auto: true,
		slideWidth: 500,
		slideMargin: 10,
		controls: false,
		maxSlides: 4
	};
	var slide01 = $('#slider01').bxSlider(config);
	var slide02 = $('#slider02').bxSlider(config);
	var slide03 = $('#slider03').bxSlider(config);
	
	$(window).on('load resize',function() {
		setTimeout(function() {
			var wid = window.innerWidth;
			if(wid > 1000) {
				slide01.reloadSlider();
				slide02.reloadSlider();
				slide03.reloadSlider();
			}else {
				slide01.destroySlider();
				slide02.destroySlider();
				slide03.destroySlider();
			}
		},10);
	});
});