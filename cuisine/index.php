<!doctype html>
<html lang="ja">
<head>
<?php include("../common/inc/head.php"); ?>
<title>Cuisine-お料理 | kitano garden</title>
<!-- ▼個別CSS▼ -->
<link rel="stylesheet" type="text/css" href="/cuisine/common/styles/cuisine.css">
<!-- ▲個別CSS▲ -->
<!-- ▼個別JS▼ -->
<script type="text/javascript" src="/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="/cuisine/common/js/cuisine.js"></script>
<!-- <script type="text/javascript" src="js/common/jquery.matchHeight.js"></script> -->
<!-- ▲個別JS▲ -->

</head>
<body>
<?php include("../common/inc/header.php"); ?>
<div id="wrapper">
    <div class="l-mv">
        <div class="l-mv-txtarea">
            <div class="l-mv-txtarea-inner">
                <h2 class="l-mv-txtarea-en">Cuisine</h2>
                <p class="l-mv-txtarea-jp">お料理</p>
            </div>
        </div>
    </div>
    <!-- / .l-mv -->
    
    <section class="l-block01">
        <div class="l-block01-inner">
            <div class="l-block01-img"><img src="/cuisine/common/img/img_block01.png" alt=""></div>
            <div class="l-block01-txtarea">
                <h3 class="l-block01-ttl">四季を彩る本格神戸フレンチ。</h3>
                <p class="l-block-ttl-sub">老舗が誇る、このうえなく美しい、<br>五感で感じるウェディングメニュー</p>
                <p class="l-block-txt">ガーデンの四季に合わせて考えられた婚礼料理は、<br class="pconly">旬の味覚と彩りを兼ね備えた、正統派のフランス料理。</p>
                <p class="l-block-txt">神戸牛をはじめ、兵庫県の素材を中心に野菜はオーガニックのものを選び、<br class="pconly">淡路や明石の新鮮な海の幸をふんだんに使用。<br class="pconly">その時期に一番旬のものを絶妙なタイミングでゲストのもとへ。</p>
                <p class="l-block-txt">美しい食器や気品あるグラス、ソムリエが厳選するワインなど<br class="pconly">細部にまでこだわりをもって、ゲストが本当に結婚式を堪能する時間が生まれます。</p>
            </div>
        </div>
    </section>
    <!-- / .l-block01 -->
    
    <section class="l-block02">
        <div class="l-block02-inner">
            <div class="l-block02-img"><img src="/cuisine/common/img/img_block02.png" alt=""></div>
            <div class="l-block02-txtarea">
                <h3 class="l-block02-ttl"><span class="l-block02-ttl-en">Menu Sample</span><span class="l-block02-ttl-jp">メニューサンプル</span></h3>
                <p class="l-block02-txt">※写真はイメージです</p>
            </div>
        </div>
    </section>
    <!-- / .l-block02 -->
    
    
    <section class="l-block03">
        <div class="l-block03-item">
            <div class="l-block03-img"><img src="/cuisine/common/img/img_block03_01.jpg" alt=""></div>
            <div class="l-block03-txtarea">
                <div class="l-block03-txtarea-inner">
                    <h4 class="l-block03-ttl">Hors-d'œuvre</h4>
                    <p class="l-block03-txt-en">Tartelettes de légumes et des fruits de mer au caviar<br>Style jardin de fleurs printanier</p>
                    <p class="l-block03-txt-jp l-mb18">彩り豊かな野菜と魚介類のタルトレット、キャビア添え<br>春のフラワーガーデンスタイル</p>
                    
                    <p class="l-block03-txt-en">Langustine et Poulet de Tamba<br>" Le bonheur dans les feuilles de Brick au printemps"<br>avec une salade de mimosa aux truffes</p>
                    <p class="l-block03-txt-jp l-mb18">ラングスティーヌと丹波地鶏<br>“春の訪れブリックの葉に包まれた幸せ”<br>トリュフ入りミモザ風サラダと共に</p>
                    
                    <p class="l-block03-txt-en">Gelée de homard Canadien et asperges vertes<br>Sauce aurore parfumée extraite de sa coquille</p>
                    <p class="l-block03-txt-jp">とっておきのオードブル<br>カナダ産オマール海老とグリーンアスパラガスのゼリー寄せ<br>その殻から抽出した香り豊かなオーロラソース</p>
                </div>
            </div>
        </div>
        <div class="l-block03-item">
            <div class="l-block03-img"><img class="is-imgChange" src="/cuisine/common/img/img_block03_02_pc.jpg" alt=""></div>
            <div class="l-block03-txtarea">
                <div class="l-block03-txtarea-inner">
                    <h4 class="l-block03-ttl">Soupe</h4>
                    <p class="l-block03-txt-en">Potage de Saint-Germain<br>écume au lard et d’huile à la menthe</p>
                    <p class="l-block03-txt-jp l-mb18">グリーンピースのポタージュ サン・ジェルマン風<br>ベーコンのエキュームとミントオイルのアクセント</p>
                    
                    <p class="l-block03-txt-en">Potage julienne d’Arblay écume au lard<br>"La bénédiction d'ange"</p>
                    <p class="l-block03-txt-jp l-mb18">新じゃがいものポタージュ、ダルブレ風、野菜のジュリエンヌ<br>“天使の祝福”</p>
                    
                    <p class="l-block03-txt-en">Trois plus grands mets Chinois de la nageoire de requin et <br>du crabe royal<br>versez du consomme d'or</p>
                    <p class="l-block03-txt-jp l-mb18">中国三大珍味のひとつフカヒレと蟹のロワイヤル<br>黄金に輝くコンソメのあんかけをそそいで</p>
                </div>
            </div>
        </div>
        <div class="l-block03-item">
            <div class="l-block03-img"><img class="is-imgChange" src="/cuisine/common/img/img_block03_03_pc.jpg" alt=""></div>
            <div class="l-block03-txtarea">
                <div class="l-block03-txtarea-inner">
                    <h4 class="l-block03-ttl">Poisson</h4>
                    <p class="l-block03-txt-en">Saumon Norvégien poêlé “parfum printanier ”<br>harmonie à la sauce Vermouth</p>
                    <p class="l-block03-txt-jp l-mb18">ノルウェー産サーモンのポワレ“春の香り”<br>ヴェルモットソースとのハーモニー</p>
                    
                    <p class="l-block03-txt-en">Sakuradai rôtie, les moules et les palourdes de l'ensemble<br>sauce champagne , Kobe légumes locaux aux primeurs</p>
                    <p class="l-block03-txt-jp l-mb18 colorGray01">兵庫県産 桜鯛のロースト、ムール貝とアサリのアンサンブル<br>シャンパンソース、神戸 春の地場 野菜添え</p>
                    
                    <p class="l-block03-txt-en">Imagé le Midi<br>Paupiette de sole arrivée de Setouchi<br>tomate confite et basilic, arôme EX d'huile d'olive vierge</p>
                    <p class="l-block03-txt-jp colorGray01">南仏をイメージしました<br>瀬戸内海産舌平目のポーピエット<br>トマトコンフィとバジルEXヴァージンオリーブオイル風味</p>
                </div>
            </div>
        </div>
        <div class="l-block03-item">
            <div class="l-block03-img"><img class="is-imgChange" src="/cuisine/common/img/img_block03_04_pc.jpg" alt=""></div>
            <div class="l-block03-txtarea">
                <div class="l-block03-txtarea-inner">
                    <h4 class="l-block03-ttl">Viande</h4>
                    <p class="l-block03-txt-en">Duo de boeufs au vin rouge,<br>Purée de pomme de terre aux truffes</p>
                    <p class="l-block03-txt-jp l-mb18 colorGray01">国産牛ロース肉のステーキと牛頬肉の赤ワイン煮のデュエット<br>トリュフ入りポテトのピューレ添え</p>
                    <p class="l-block03-txt-en">Steak filet de bœuf“Parisienne ”<br>Gratinée d'asperges blanches, sauce porto </p>
                    <p class="l-block03-txt-jp l-mb18 colorGray01">国産 特選牛フィレ肉ステーキ“パリジェンヌ”<br>ホワイトアスパラガスのグラティネ芳醇なポルト酒ソース</p>
                    <p class="l-block03-txt-en">Filet de bœuf Japonais et Foie gras sautés à la Rossini<br>  Bell Epoque traditionnelle “Style Kitano Garden”</p>
                    <p class="l-block03-txt-jp colorGray01">伝統の味 黒毛和牛フィレ肉とフォワグラのソテー、ロッシーニ風<br>至福の時、高貴なトリュフソース　ベル・エポック<br>“北野ガーデンスタイル”</p>
                </div>
            </div>
        </div>
    </section>
    <!-- / .l-block03 -->
    
    <section class="l-block04">
        <h3 class="l-block04-ttl"><span class="l-block04-ttl-en">Gallery</span><span class="l-block04-ttl-jp">お料理ギャラリー</span></h3>
        <div class="l-block04-slide-wrap">
            <ul class="l-block04-slide" id="gallery">
                <li>
                    <div class="l-block04-slide-img"><img class="is-imgChange" src="/cuisine/common/img/img_block04_01_pc.jpg" alt=""></div>
                    <p class="l-block04-slide-txt">１行説明テキストが入ります。１行説明テキストが入ります。</p>
                </li>
                <li>
                    <div class="l-block04-slide-img"><img class="is-imgChange" src="/cuisine/common/img/img_block04_02_pc.jpg" alt=""></div>
                    <p class="l-block04-slide-txt">１行説明テキストが入ります。１行説明テキストが入ります。</p>
                </li>
                <li>
                    <div class="l-block04-slide-img"><img class="is-imgChange" src="/cuisine/common/img/img_block04_03_pc.jpg" alt=""></div>
                    <p class="l-block04-slide-txt">１行説明テキストが入ります。１行説明テキストが入ります。</p>
                </li>
                <li>
                    <div class="l-block04-slide-img"><img class="is-imgChange" src="/cuisine/common/img/img_block04_04_pc.jpg" alt=""></div>
                    <p class="l-block04-slide-txt">１行説明テキストが入ります。１行説明テキストが入ります。</p>
                </li>
            </ul>
        </div>
    </section>
    <!-- / .l-block04 -->
    <?php include("../common/inc/pickupfair.php"); ?>
    
</div>
<!-- / #wrapper -->
<?php include("../common/inc/footer.php"); ?>
</body>
</html>
