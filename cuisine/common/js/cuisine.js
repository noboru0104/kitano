$(function() {
	var config = {
		slideMargin: 20,
		slideWidth: 500,
		moveSlides: 1,
		maxSlides: 2,
		minSlides: 1,
		controls: false
	}
	var gallery = $('#gallery').bxSlider(config);
	$(window).on('load resize',function() {
		if(window.innerWidth <= 1000) {
			config['maxSlides'] = 1;
			config['slideWidth'] = 0;
		}else {
			config['maxSlides'] = 2;
			config['slideWidth'] = 500;
		}
		gallery.reloadSlider();
	});
	
});